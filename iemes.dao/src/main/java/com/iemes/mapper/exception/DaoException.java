package com.iemes.mapper.exception;

/**
 * 业务层异常
 * @author huahao
 *
 */
public class DaoException extends RuntimeException {

	private static final long serialVersionUID = 6369672749671888974L;
	
	public DaoException(String str) {
        super(str);
    }

}
