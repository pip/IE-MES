package com.iemes.service.impl.work_process;

import java.util.List;

import javax.inject.Inject;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import com.iemes.entity.PodFunctionFormMap;
import com.iemes.entity.UserFormMap;
import com.iemes.exception.BusinessException;
import com.iemes.mapper.BaseExtMapper;
import com.iemes.service.work_process.PodFunctionService;
import com.iemes.util.ListUtils;
import com.iemes.util.ShiroSecurityHelper;
import com.iemes.util.UUIDUtils;

@Service
public class PodFunctionServiceImpl implements PodFunctionService {
	
	private Logger log = Logger.getLogger(this.getClass());
			
	@Inject
	private BaseExtMapper baseMapper;

	@Override
	@Transactional
	public void savePodFunction(PodFunctionFormMap podFunctionFormMap) throws BusinessException {
		try {
			String id = podFunctionFormMap.getStr("id");
			String podFunctionNo = podFunctionFormMap.getStr("pod_function_no");
			
			if(StringUtils.isEmpty(podFunctionNo)) {
				throw new BusinessException("POD功能编号不能为空，请输入！");
			}
			
			PodFunctionFormMap podFunctionFormMap2 = new PodFunctionFormMap();
			podFunctionFormMap2.put("pod_function_no", podFunctionNo);
			podFunctionFormMap2.put("site_id", ShiroSecurityHelper.getSiteId());
			List<PodFunctionFormMap> list = baseMapper.findByNames(podFunctionFormMap2);
			
			//如果编号存在，就执行更新，不存在就执行新增
			if (ListUtils.isNotNull(list)) {
				//更新 
				PodFunctionFormMap podFunctionFormMap3 = new PodFunctionFormMap();
				podFunctionFormMap3 = list.get(0);
				id = podFunctionFormMap3.getStr("id");
				podFunctionFormMap.set("id", id);
				
				baseMapper.editEntity(podFunctionFormMap);
			}else {
				//新增
				podFunctionFormMap.put("id", UUIDUtils.getUUID());
				baseMapper.addEntity(podFunctionFormMap);
			}
			
		} catch (Exception e) {
			log.error(e.getMessage(), e);
			throw new BusinessException("POD功能保存失败！"+e.getMessage());
		}
	}

	@Override
	@Transactional
	public void delPodFunction(String podFunctionId) throws BusinessException {
		try {
			baseMapper.deleteByAttribute("id", podFunctionId, PodFunctionFormMap.class);
		} catch (Exception e) {
			log.error(e.getMessage(), e);
			throw new BusinessException("POD功能删除失败！"+e.getMessage());
		}
	}

}
