package com.iemes.service.impl.workcenter_model;

import java.util.List;

import javax.inject.Inject;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import com.iemes.entity.ContainerSFCFormMap;
import com.iemes.entity.ContainerTypeFormMap;
import com.iemes.entity.UDefinedDataValueFormMap;
import com.iemes.entity.UserFormMap;
import com.iemes.exception.BusinessException;
import com.iemes.mapper.BaseExtMapper;
import com.iemes.service.CommonService;
import com.iemes.service.workcenter_model.ContainerService;
import com.iemes.util.ListUtils;
import com.iemes.util.ShiroSecurityHelper;
import com.iemes.util.UUIDUtils;

@Service
public class ContainerServiceImpl implements ContainerService {
	
	private Logger log = Logger.getLogger(this.getClass());

	@Inject
	private CommonService commonService;	
	
	@Inject
	private BaseExtMapper baseMapper;

	@Override
	@Transactional
	public void saveContainer(ContainerTypeFormMap containerFormMap) throws BusinessException {
		try {
			String id = containerFormMap.getStr("id");
			String containerTypeNo = containerFormMap.getStr("container_type_no");
			
			if(StringUtils.isEmpty(containerTypeNo)) {
				throw new BusinessException("容器编号不能为空，请输入！");
			}
			
			ContainerTypeFormMap containerFormMap2 = new ContainerTypeFormMap();
			containerFormMap2.put("container_type_no", containerTypeNo);
			containerFormMap2.put("site", ShiroSecurityHelper.getSiteId());
			List<ContainerTypeFormMap> list = baseMapper.findByNames(containerFormMap2);
			
			//如果编号存在，就执行更新，不存在就执行新增
			if (ListUtils.isNotNull(list)) {
				//更新 
				ContainerTypeFormMap containerFormMap3 = new ContainerTypeFormMap();
				containerFormMap3 = list.get(0);
				id = containerFormMap3.getStr("id");
				containerFormMap.set("id", id);
				
				baseMapper.editEntity(containerFormMap);
			}else {
				//新增
				containerFormMap.put("id", UUIDUtils.getUUID());
				baseMapper.addEntity(containerFormMap);
			}
			
			//保存自定义数据
			commonService.saveUDefinedDataValueS(containerFormMap.getStr("id"), containerFormMap.getStr("containerUdata"));
		} catch (Exception e) {
			log.error(e.getMessage(), e);
			throw new BusinessException("容器保存失败！"+e.getMessage());
		}
	}

	@Override
	@Transactional
	public void delContainer(String containerId) throws BusinessException {
		try {
			// 1.如果容器已用于包装，则不允许删除
			ContainerSFCFormMap containerSFCFormMap = new ContainerSFCFormMap();
			containerSFCFormMap.put("container_id", containerId);
			List<ContainerSFCFormMap> listContainerSFCFormMap= baseMapper.findByNames(containerSFCFormMap);
			if (ListUtils.isNotNull(listContainerSFCFormMap)) {
				throw new BusinessException("此容器已用于包装，不可删除！");
			}
			//删除容器信息
			baseMapper.deleteByAttribute("id", containerId, UserFormMap.class);
			
			//删除自定义数据信息
			baseMapper.deleteByAttribute("data_type_detail_id", containerId, UDefinedDataValueFormMap.class);
		} catch (Exception e) {
			log.error(e.getMessage(), e);
			throw new BusinessException("容器删除失败！"+e.getMessage());
		}
	}

}
