package com.iemes.service.impl.workcenter_model;

import java.util.List;

import javax.inject.Inject;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import com.iemes.entity.ShopOrderSfcFormMap;
import com.iemes.entity.SiteFormMap;
import com.iemes.entity.WorkCenterFormMap;
import com.iemes.entity.WorkResourceFormMap;
import com.iemes.exception.BusinessException;
import com.iemes.mapper.BaseExtMapper;
import com.iemes.service.CommonService;
import com.iemes.service.workcenter_model.WorkCenterService;
import com.iemes.util.ListUtils;
import com.iemes.util.ShiroSecurityHelper;
import com.iemes.util.UUIDUtils;

@Service
public class WorkCenterServiceImpl implements WorkCenterService {

	@Inject
	private BaseExtMapper baseMapper;
	
	@Inject
	private CommonService commonService;
	
	private Logger log = Logger.getLogger(this.getClass());
	
	@Override
	@Transactional
	public void saveWorkCenter(WorkCenterFormMap workCenterFormMap) throws BusinessException {
		try {
			String id = workCenterFormMap.getStr("id");
			String workCenterNo = workCenterFormMap.getStr("workcenter_no");
			String version = workCenterFormMap.getStr("workcenter_version");
			
			if(StringUtils.isEmpty(workCenterNo)) {
				throw new BusinessException("工作中心编号不能为空，请输入！");
			}	
			if(StringUtils.isEmpty(version)) {
				throw new BusinessException("工作中心版本号不能为空，请输入！");
			}
			
			WorkCenterFormMap workCenterFormMap2 = new WorkCenterFormMap();
			workCenterFormMap2.put("site_id", ShiroSecurityHelper.getSiteId());
			workCenterFormMap2.put("workcenter_no", workCenterNo);
			workCenterFormMap2.put("workcenter_version", version);
			List<WorkCenterFormMap> list = baseMapper.findByNames(workCenterFormMap2);
			
			//如果编号存在，就执行更新，不存在就执行新增
			if (ListUtils.isNotNull(list)) {
				//更新 
				WorkCenterFormMap workCenterFormMap3 = new WorkCenterFormMap();
				workCenterFormMap3 = list.get(0);
				id = workCenterFormMap3.getStr("id");
				workCenterFormMap.set("id", id);
				
				baseMapper.editEntity(workCenterFormMap);
			}else {
				//新增
				id = UUIDUtils.getUUID();
				workCenterFormMap.put("id", id);
				baseMapper.addEntity(workCenterFormMap);
			}
			
			// .添加或更新自定义数据信息
			String dataValue = workCenterFormMap.getStr("udefined_data");
			commonService.saveUDefinedDataValueS(id, dataValue);
		} catch (Exception e) {
			log.error(e.getMessage(), e);
			throw new BusinessException("工作中心保存失败！"+e.getMessage());
		}
	}

	@Override
	@Transactional
	public void delWorkCenter(String workCenterId) throws BusinessException {
		try {
			//1.如果该工作中心有子工作中心，则不允许删除
			WorkCenterFormMap workCenterFormMap = new WorkCenterFormMap();
			workCenterFormMap.put("workcenter_parent_id", workCenterId);
			List<WorkCenterFormMap> listChildWorkCenterFormMap = baseMapper.findByNames(workCenterFormMap);
			if(ListUtils.isNotNull(listChildWorkCenterFormMap)) {	
				String chlidWorkCenterName = "";
				for(int i=0;i<listChildWorkCenterFormMap.size();i++) {
					WorkCenterFormMap map = listChildWorkCenterFormMap.get(i);
					if(StringUtils.isEmpty(chlidWorkCenterName)) {
						chlidWorkCenterName = map.getStr("workcenter_name");
					}else {
						chlidWorkCenterName += "," + map.getStr("workcenter_name");
					}
				}
				throw new BusinessException("此工作中心含有子工作中心 " + chlidWorkCenterName + " ，无法执行删除！");
			}
			
			//2.如果该工作中心（产线）已在工单中，则不允许删除
			ShopOrderSfcFormMap shopOrderSfcFormMap = new ShopOrderSfcFormMap();
			shopOrderSfcFormMap.put("where", " where workline_id ='"+ workCenterId + "' or workshop_id ='"+ workCenterId +"'");
			List<ShopOrderSfcFormMap> listShopOrderSfcFormMap = baseMapper.findByWhere(shopOrderSfcFormMap);
			if (ListUtils.isNotNull(listShopOrderSfcFormMap)) {
				String bindShoporderSfcName = "";
				for(int i=0;i<listShopOrderSfcFormMap.size();i++) {
					ShopOrderSfcFormMap map = listShopOrderSfcFormMap.get(i);
					if(StringUtils.isEmpty(bindShoporderSfcName)) {
						bindShoporderSfcName = map.getStr("sfc");
					}else {
						bindShoporderSfcName += "," + map.getStr("sfc");
					}
				}
				throw new BusinessException("此工作中心内绑定了sfc " + bindShoporderSfcName + " ，无法执行删除！");
			}
			
			//3.如果该工作中心绑定了资源，则不允许删除
			List<WorkResourceFormMap> listWorkResourceFormMap = baseMapper.findByAttribute("workline_id", workCenterId, WorkResourceFormMap.class);
			if (ListUtils.isNotNull(listWorkResourceFormMap)) {
				String bindWorkResourceNo = "";
				for(int i=0;i<listWorkResourceFormMap.size();i++) {
					WorkResourceFormMap map = listWorkResourceFormMap.get(i);
					if(StringUtils.isEmpty(bindWorkResourceNo)) {
						bindWorkResourceNo = map.getStr("resource_no");
					}else {
						bindWorkResourceNo += "," + map.getStr("resource_no");
					}
				}
				throw new BusinessException("此工作中心内有资源 " + bindWorkResourceNo + "，无法执行删除！");
			}
			
			//4.执行删除
			baseMapper.deleteByAttribute("id", workCenterId, WorkCenterFormMap.class);
		} catch (Exception e) {
			log.error(e.getMessage(), e);
			throw new BusinessException("工作中心删除失败！"+e.getMessage());
		}
	}

}

