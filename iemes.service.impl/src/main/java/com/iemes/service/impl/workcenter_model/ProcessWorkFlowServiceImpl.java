package com.iemes.service.impl.workcenter_model;

import java.util.List;

import javax.inject.Inject;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import com.iemes.entity.FlowStepFormMap;
import com.iemes.entity.OperationFormMap;
import com.iemes.entity.ProcessWorkFlowFormMap;
import com.iemes.entity.ShoporderFormMap;
import com.iemes.entity.WorkCenterFormMap;
import com.iemes.exception.BusinessException;
import com.iemes.mapper.BaseExtMapper;
import com.iemes.service.workcenter_model.ProcessWorkFlowService;
import com.iemes.util.DateUtils;
import com.iemes.util.ListUtils;
import com.iemes.util.ShiroSecurityHelper;
import com.iemes.util.UUIDUtils;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

@Service
public class ProcessWorkFlowServiceImpl implements ProcessWorkFlowService {

	@Inject
	private BaseExtMapper baseMapper;
	
	private Logger log = Logger.getLogger(this.getClass());
	
	@Override
	public List<OperationFormMap> getAllOperations()throws Exception {
		OperationFormMap operationFormMap = new OperationFormMap();
		operationFormMap.put("site_id", ShiroSecurityHelper.getSiteId());
		return baseMapper.findByNames(operationFormMap);
	}

	@Override
	@Transactional
	public void saveProcessWorkFlow(ProcessWorkFlowFormMap processWorkFlowFormMap) throws BusinessException {
		try {
			String id = processWorkFlowFormMap.getStr("id");
			String processWorkflow = processWorkFlowFormMap.getStr("process_workflow");
			String version = processWorkFlowFormMap.getStr("process_workflow_version");
			
			if(StringUtils.isEmpty(processWorkflow)) {
				throw new BusinessException("工艺路线编号不能为空，请输入！");
			}	
			if(StringUtils.isEmpty(version)) {
				throw new BusinessException("工艺路线版本号不能为空，请输入！");
			}
			
			ProcessWorkFlowFormMap processWorkFlowFormMap2 = new ProcessWorkFlowFormMap();
			processWorkFlowFormMap2.put("process_workflow", processWorkflow);
			processWorkFlowFormMap2.put("site_id", ShiroSecurityHelper.getSiteId());
			processWorkFlowFormMap2.put("process_workflow_version", version);
			List<ProcessWorkFlowFormMap> list = baseMapper.findByNames(processWorkFlowFormMap2);
			
			//如果编号存在，就执行更新，不存在就执行新增
			if (ListUtils.isNotNull(list)) {
				//更新 
				ProcessWorkFlowFormMap processWorkFlowFormMap3 = new ProcessWorkFlowFormMap();
				processWorkFlowFormMap3 = list.get(0);
				id = processWorkFlowFormMap3.getStr("id");
				processWorkFlowFormMap.set("id", id);
				
				baseMapper.editEntity(processWorkFlowFormMap);
			}else {
				//新增
				id = UUIDUtils.getUUID();
				processWorkFlowFormMap.put("id", id);
				baseMapper.addEntity(processWorkFlowFormMap);
			}
			
			//保存或更新工艺路线步骤信息
			baseMapper.deleteByAttribute("process_workflow_id", processWorkFlowFormMap.getStr("id"), FlowStepFormMap.class);
			
			String flowData = processWorkFlowFormMap.getStr("flowData");
			JSONArray jsonArray = JSONArray.fromObject(flowData);
			for (int i=0;i<jsonArray.size();i++) {
				JSONObject jsonObject = jsonArray.getJSONObject(i);
				
				FlowStepFormMap flowStepFormMap = new FlowStepFormMap();
				flowStepFormMap.put("id", UUIDUtils.getUUID());
				flowStepFormMap.put("process_workflow_id", processWorkFlowFormMap.getStr("id"));
				flowStepFormMap.put("operation_id", jsonObject.getString("from"));
				flowStepFormMap.put("next_operation_id", jsonObject.getString("to"));
				flowStepFormMap.put("status", jsonObject.getString("from_status"));
				flowStepFormMap.put("site_id", ShiroSecurityHelper.getSiteId());
				flowStepFormMap.put("create_user", ShiroSecurityHelper.getCurrentUsername());
				flowStepFormMap.put("create_time", DateUtils.getStringDateTime());
				flowStepFormMap.put("process_workflow_version", processWorkFlowFormMap.getStr("process_workflow_version"));
				baseMapper.addEntity(flowStepFormMap);
			}
			
		} catch (Exception e) {
			log.error(e.getMessage(), e);
			throw new BusinessException("工艺路线保存失败！"+e.getMessage());
		}
	}

	@Override
	@Transactional
	public void delProcessWorkFlow(String ProcessWorkFlowId) throws BusinessException {
		try {
			//1.如果工艺路线在工单中，则不允许删除
			ShoporderFormMap shoporderFormMap = new ShoporderFormMap();
			shoporderFormMap.put("process_workflow_id", ProcessWorkFlowId);
			List<ShoporderFormMap> listShoporderFormMap = baseMapper.findByNames(shoporderFormMap);
			if (ListUtils.isNotNull(listShoporderFormMap)) {
				String bindShoporderNo = "";
				for(int i=0;i<listShoporderFormMap.size();i++) {
					ShoporderFormMap map = listShoporderFormMap.get(i);
					if(StringUtils.isEmpty(bindShoporderNo)) {
						bindShoporderNo = map.getStr("shoporder_no");
					}else {
						bindShoporderNo += "," + map.getStr("shoporder_no");
					}
				}
				throw new BusinessException("此工艺路线已绑定了工单 " + bindShoporderNo + "，无法执行删除！");				
			}
				
			//删除操作信息
			baseMapper.deleteByAttribute("id", ProcessWorkFlowId, ProcessWorkFlowFormMap.class);
			
			//删除工艺路线步骤信息
			baseMapper.deleteByAttribute("process_workflow_id", ProcessWorkFlowId, FlowStepFormMap.class);
		} catch (Exception e) {
			log.error(e.getMessage(), e);
			throw new BusinessException("工艺路线删除失败！"+e.getMessage());
		}
	}

}
