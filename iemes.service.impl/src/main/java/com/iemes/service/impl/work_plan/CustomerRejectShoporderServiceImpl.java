package com.iemes.service.impl.work_plan;

import java.util.List;

import javax.inject.Inject;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import com.iemes.entity.CustomerRejectSfcInfoFormMap;
import com.iemes.entity.ShoporderFormMap;
import com.iemes.exception.BusinessException;
import com.iemes.mapper.base.BaseMapper;
import com.iemes.service.work_plan.CustomerRejectShoporderService;
import com.iemes.util.ListUtils;
import com.iemes.util.ShiroSecurityHelper;

@Service
public class CustomerRejectShoporderServiceImpl implements CustomerRejectShoporderService {

	@Inject
	private BaseMapper baseMapper;
	
	private Logger log = Logger.getLogger(CustomerRejectShoporderServiceImpl.class);
	
	@Override
	@Transactional
	public void saveCustomerRejectShoporder(ShoporderFormMap shoporderFormMap, List<CustomerRejectSfcInfoFormMap> list)throws BusinessException {
		try {
			String shoporder_no = shoporderFormMap.getStr("shoporder_no");
			if (StringUtils.isEmpty(shoporder_no)) {
				throw new BusinessException("客退单号不能为空！！！");
			}
			ShoporderFormMap shoporderFormMap2 = new ShoporderFormMap();
			shoporderFormMap2.put("site_id", ShiroSecurityHelper.getSiteId());
			shoporderFormMap2.put("shoporder_no", shoporder_no);
			shoporderFormMap2.put("shoporder_type", "rework");
			List<ShoporderFormMap> listShoporderFormMap = baseMapper.findByNames(shoporderFormMap2);
			if (ListUtils.isNotNull(listShoporderFormMap)) {
				baseMapper.editEntity(shoporderFormMap);
				for (CustomerRejectSfcInfoFormMap custom : list) {
					baseMapper.deleteByNames(custom);
				}
				baseMapper.batchSave(list);
			}else {
				baseMapper.addEntity(shoporderFormMap);
				baseMapper.batchSave(list);
			}
		}catch(Exception e) {
			log.error(e.getMessage());
			throw new BusinessException(e.getMessage());
		}
	}

	@Override
	@Transactional
	public void delCustomerRejectShoporder(ShoporderFormMap shoporderFormMap, List<CustomerRejectSfcInfoFormMap> list)
			throws BusinessException {
		try {
			baseMapper.deleteByNames(shoporderFormMap);
			for (CustomerRejectSfcInfoFormMap custom : list) {
				baseMapper.deleteByNames(custom);
			}
		}catch(Exception e) {
			log.error(e.getMessage());
			throw new BusinessException(e.getMessage());
		}
	}

}
