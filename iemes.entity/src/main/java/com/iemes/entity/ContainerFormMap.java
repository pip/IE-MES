package com.iemes.entity;

import com.iemes.entity.annotation.TableSeg;
import com.iemes.entity.FormMap;

@TableSeg(tableName = "mds_container", id="id")
public class ContainerFormMap extends FormMap<String,Object>{

	/**
	 * 
	 */
	private static final long serialVersionUID = -3758359975867129684L;

}
