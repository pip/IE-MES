package com.iemes.entity;

import com.iemes.entity.annotation.TableSeg;
import com.iemes.entity.FormMap;

/**
 * 自定义数据表实体类
 * @author Administrator
 *
 */
@TableSeg(tableName = "mds_udefined_data", id="id")
public class UDefinedDataFormMap extends FormMap<String,Object> {

	/**
	 * 
	 */
	private static final long serialVersionUID = -4881148610747701155L;

}
