package com.iemes.entity;

import com.iemes.entity.annotation.TableSeg;
import com.iemes.entity.FormMap;



/**
 * 工艺路线实体表
 */
@TableSeg(tableName = "mds_process_workflow", id="id")
public class ProcessWorkFlowFormMap extends FormMap<String,Object>{

	private static final long serialVersionUID = -6801789682772196454L;

}
