package com.iemes.entity;

import com.iemes.entity.annotation.TableSeg;
import com.iemes.entity.FormMap;



/**
 * mds_operation_pod实体表
 */
@TableSeg(tableName = "mds_operation_pod", id="id")
public class OperationPodFormMap extends FormMap<String,Object>{

	/**
	 *@descript
	 *@author MDS
	 *@version 2.0
	 */
	private static final long serialVersionUID = 1L;
	
}
