package com.iemes.entity;

import com.iemes.entity.annotation.TableSeg;
import com.iemes.entity.FormMap;

@TableSeg(tableName = "mds_work_resource", id="id")
public class WorkResourceFormMap extends FormMap<String,Object>{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

}
