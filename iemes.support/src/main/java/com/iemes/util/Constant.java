package com.iemes.util;

public interface Constant {

	public static final String SUCCESS = "success";
	
	public static final String ERROR = "error";
	
	public static final String 开始 = "开始";

	public static final String 完成 = "完成";
	
	public static final String 装配 = "装配";
	
	public static final String 记录不良 = "记录不良";
	
	public static final String SAVE = "save";
	
	public static final String UPDATE = "update";
}
