package com.iemes.util;

import java.util.ArrayList;
import java.util.List;

/**
 * 这个是列表树形式显示的实体, 这里的字段是在前台显示所有的,可修改
 * 
 * @author MDS
 */
public class TreeObject {
	private String id;
	private String parent_id;
	private String res_name;
	private String parent_name;
	private String res_key;
	private String res_url;
	private Integer level;
	private String type;
	private String description;
	private String icon;
	private Integer is_hide;
	private String is_check = "0";
	
	public String getIs_check() {
		return is_check;
	}

	public void setIs_check(String is_check) {
		this.is_check = is_check;
	}

	private List<TreeObject> children = new ArrayList<TreeObject>();

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getParent_id() {
		return parent_id;
	}

	public void setParent_id(String parent_id) {
		this.parent_id = parent_id;
	}

	public String getRes_name() {
		return res_name;
	}

	public void setRes_name(String res_name) {
		this.res_name = res_name;
	}

	public String getParent_name() {
		return parent_name;
	}

	public void setParent_name(String parent_name) {
		this.parent_name = parent_name;
	}

	public String getRes_key() {
		return res_key;
	}

	public void setRes_key(String res_key) {
		this.res_key = res_key;
	}

	public String getRes_url() {
		return res_url;
	}

	public void setRes_url(String res_url) {
		this.res_url = res_url;
	}

	public Integer getLevel() {
		return level;
	}

	public void setLevel(Integer level) {
		this.level = level;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getIcon() {
		return icon;
	}

	public void setIcon(String icon) {
		this.icon = icon;
	}

	public Integer getIs_hide() {
		return is_hide;
	}

	public void setIs_hide(Integer is_hide) {
		this.is_hide = is_hide;
	}

	public List<TreeObject> getChildren() {
		return children;
	}

	public void setChildren(List<TreeObject> children) {
		this.children = children;
	}

}
