package com.iemes.service.system;

import com.iemes.entity.UserFormMap;
import com.iemes.exception.BusinessException;

public interface UserService {

	/**
	 * 保存用户（新增或更新）
	 * @param userFormMap
	 * @throws BusinessException
	 */
	String saveUser(UserFormMap userFormMap)throws BusinessException;
	
	/**
	 * 删除用户
	 * @param userFormMap
	 * @throws BusinessException
	 */
	void delUser(String id)throws BusinessException;
	
	/**
	 * 重置密码
	 * @param userFormMap
	 * @return
	 * @throws BusinessException
	 */
	void resetPass(UserFormMap userFormMap)throws BusinessException;
	
	/**
	 * 修改密码
	 * @param userFormMap
	 * @throws BusinessException
	 */
	void updatePass(UserFormMap userFormMap)throws BusinessException;
}
