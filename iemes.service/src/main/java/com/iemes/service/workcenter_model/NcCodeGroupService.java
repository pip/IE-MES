package com.iemes.service.workcenter_model;

import com.iemes.entity.NcCodeGroupFormMap;
import com.iemes.exception.BusinessException;

public interface NcCodeGroupService {
	
	/**
	 * 保存不良代码
	 * @param ncCodeGroupFormMap
	 * @throws BusinessException
	 */
	void saveNcCodeGroup(NcCodeGroupFormMap ncCodeGroupFormMap)throws BusinessException;

	/**
	 * 删除不良代码
	 * @param ncCodeGroupId
	 * @throws BusinessException
	 */
	void delNcCodeGroup(String ncCodeGroupId)throws BusinessException;
}
