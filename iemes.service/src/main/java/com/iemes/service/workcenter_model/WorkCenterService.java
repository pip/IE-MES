package com.iemes.service.workcenter_model;

import com.iemes.entity.WorkCenterFormMap;
import com.iemes.exception.BusinessException;

public interface WorkCenterService {
	/**
	 * 保存工作中心
	 * @param workCenterFormMap
	 * @throws BusinessException
	 */
	void saveWorkCenter(WorkCenterFormMap workCenterFormMap)throws BusinessException;

	/**
	 * 删除工作中心
	 * @param workCenterId
	 * @throws BusinessException
	 */
	void delWorkCenter(String workCenterId)throws BusinessException;
}
