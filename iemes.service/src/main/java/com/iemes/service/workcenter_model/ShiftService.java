package com.iemes.service.workcenter_model;

import com.iemes.entity.ShiftFormMap;
import com.iemes.exception.BusinessException;

public interface ShiftService {
	
	/**
	 * 保存班次
	 * @param shiftFormMap
	 * @throws BusinessException
	 */
	void saveShift(ShiftFormMap shiftFormMap)throws BusinessException;

	/**
	 * 删除班次
	 * @param shiftId
	 * @throws BusinessException
	 */
	void delShift(String shiftId)throws BusinessException;
}
