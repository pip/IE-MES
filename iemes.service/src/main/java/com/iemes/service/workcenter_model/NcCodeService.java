package com.iemes.service.workcenter_model;

import com.iemes.entity.NcCodeFormMap;
import com.iemes.exception.BusinessException;

public interface NcCodeService {
	
	/**
	 * 保存不良代码
	 * @param ncCodeFormMap
	 * @throws BusinessException
	 */
	void saveNcCode(NcCodeFormMap ncCodeFormMap)throws BusinessException;

	/**
	 * 删除不良代码
	 * @param ncCodeId
	 * @throws BusinessException
	 */
	void delNcCode(String ncCodeId)throws BusinessException;
}
