package com.iemes.service;

import java.util.List;

import com.iemes.entity.FlowStepFormMap;
import com.iemes.entity.SfcStepFormMap;
import com.iemes.entity.ShoporderFormMap;
import com.iemes.exception.BusinessException;

/**
 * 公共业务方法
 * @author Administrator
 *
 */
public interface CommonService {

	/**
	 * 保存自定义数据的值
	 * @param id
	 * @param formMap
	 * @throws BusinessException
	 */
	void saveUDefinedDataValueS(String id, String userDefinedValues)throws BusinessException;
	
	/**
	 * 工单状态转换
	 * @param status
	 * @throws BusinessException
	 */
	String getShoporderStatusStr(int status)throws BusinessException;
	
	/**
	 * sfc状态转换
	 * @param status
	 * @throws BusinessException
	 */
	String getSfcStatusStr(int status)throws BusinessException;
	
	/**
	 * sfc步骤状态转换
	 * @param status
	 * @throws BusinessException
	 */
	String getSfcStepStatusStr(int status)throws BusinessException;
	
	/**
	 * 根据操作查找下一步操作
	 * @param operation_id	操作ID
	 * @param process_workflow_id	工作路线ID
	 * @return
	 * @throws BusinessException
	 */
	List<FlowStepFormMap> getNextOperation(String operation_id, String process_workflow_id)throws BusinessException;
	
	/**
	 * 根据操作查找下一步维修操作
	 * @param operation_id	操作ID
	 * @param process_workflow_id	工作路线ID
	 * @return
	 * @throws BusinessException
	 */
	List<FlowStepFormMap> getNextRepairOperation(String operation_id, String process_workflow_id)throws BusinessException;
	
	/**
	 * 是否为工单的最后一个SFC（请先修改shoporder_sfc表后再调用该方法）
	 * @param shoporder_id
	 * @return
	 * @throws BusinessException
	 */
	ShoporderFormMap isLastSfcOnShoporder(String shoporder_id)throws BusinessException;
	
	/**
	 * 获取SFC的最后一个步骤
	 * @param sfc
	 * @return
	 * @throws BusinessException
	 */
	SfcStepFormMap getLastSfcStep(String sfc)throws BusinessException;
	
}
