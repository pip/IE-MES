package com.iemes.service.WorkshopInventory;

import com.iemes.entity.WorkShopInventoryFormMap;
import com.iemes.exception.BusinessException;

public interface WorkshopInventoryReceptionService {
	
	/**
	 * 保存工单
	 * @param workshopInventoryFormMap
	 * @throws BusinessException
	 */
	void receptItem(WorkShopInventoryFormMap workshopInventoryFormMap)throws BusinessException;
}
