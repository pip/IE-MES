package com.iemes.service.work_process;

import com.iemes.entity.PodButtonFormMap;
import com.iemes.exception.BusinessException;

public interface PodButtonService {

	/**
	 * 保存POD功能
	 * @param podButtonFormMap
	 * @throws BusinessException
	 */
	void savePodButton(PodButtonFormMap podButtonFormMap)throws BusinessException;

	/**
	 * 删除POD功能
	 * @param podButtonId
	 * @throws BusinessException
	 */
	void delPodButton(String podButtonId)throws BusinessException;
}
