package com.iemes.service.work_plan;

import java.util.List;

import com.iemes.entity.CustomerRejectSfcInfoFormMap;
import com.iemes.entity.ShoporderFormMap;
import com.iemes.exception.BusinessException;

public interface CustomerRejectShoporderService {

	/**
	 * 保存客退单
	 * @param shoporderFormMap
	 */
	void saveCustomerRejectShoporder(ShoporderFormMap shoporderFormMap, List<CustomerRejectSfcInfoFormMap> list)throws BusinessException;
	
	/**
	 * 删除客退单
	 * @param shoporderFormMap
	 * @throws BusinessException
	 */
	void delCustomerRejectShoporder(ShoporderFormMap shoporderFormMap, List<CustomerRejectSfcInfoFormMap> list)throws BusinessException;
}
