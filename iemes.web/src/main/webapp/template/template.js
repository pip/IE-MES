$(document).ready(function() {
	
	if (!stringIsNull($('#errorMessage').val())) {
		showErrorNoticeMessage($('#errorMessage').val());
	}
	
	if (!stringIsNull($('#successMessage').val())) {
		showSuccessNoticeMessage($('#successMessage').val());
	}
	var page_res_id = $('#page_res_id').val();
	
	//保存
	$("form").submit(function() {
		var data = $("form").serialize();
		var url = $('#btn_save').attr("data-url");
		$.post(rootPath + url, data, function(e, status, xhr){
			var data= JSON.parse(e);
			if (data.status) {
				showSuccessNoticeMessage(data.message);
			}else {
				showErrorNoticeMessage(data.message);
			}
		},"json")
		return false;
	});
	
	//检索
	$('#btn_search').click(function (){
		var url = $(this).attr("data-url");
		var data_id = $('#data_id').val();
		
		var tb = $(".index_centent");
		tb.empty();
		tb.load(rootPath + url + "?data_id="+data_id+"&page_res_id="+page_res_id);
	})
	
	//清除
	$('#btn_clean').click(function (){
		var url = $(this).attr("data-url");
		var tb = $(".index_centent");
		tb.empty();
		tb.load(rootPath + url + "?page_res_id="+page_res_id);
	})
	
	//删除
	$('#btn_del').click(function (){
		var url = $(this).attr("data-url");
		$.MsgBox.Confirm("","该条数据将会被删除，确定继续吗？",function (){
			var data_id = $('#data_id').val();
			
			var tb = $(".index_centent");
			tb.empty();
			tb.load(rootPath + url + "?data_id="+data_id+"&page_res_id="+page_res_id);
		});
	})
})