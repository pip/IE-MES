<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>	
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<link rel="shortcut icon"
	href="${pageContext.servletContext.contextPath }/images/favicon.png"
	type="image/x-icon" />
<title>工单创建</title>

<script type="text/javascript" src="js/common/form_validate.js"></script>
<script type="text/javascript" src="js/common/jtab.js"></script>
<script type="text/javascript" src="js/common/mtable.js"></script>

<script type="text/javascript" src="js/work_plan/shoporder_maintenance/shoporder_maintenance.js"></script>

</head>
<body>
<form class="mds_tab_form">
	<div id="commanButtonsDiv" class=iemes_style_commanButtonsDiv>
			<c:forEach var="resRow" items="${res}">
				<c:if test="${resRow.type==2 && resRow.is_hide!=1}">
					<c:choose>
						<c:when test="${resRow.res_url=='btnSave'}">
							<input type="submit" id="btnSave" value="${resRow.res_name}">
						</c:when>
						<c:otherwise>
							<input type="button" id="${resRow.res_url}"
								value="${resRow.res_name}">
						</c:otherwise>
					</c:choose>
				</c:if>
			</c:forEach>
	</div>
	
	<div class="hidden">
		<input type="hidden" value="${page_res_id}" id="page_res_id">
		<input type="hidden" name="shoporderFormMap.id" value="${shoporderFormMap.id}" id="shoporder_id">
		<input type="hidden" value="${errorMessage}" id="errorMessage">
		<input type="hidden" value="${successMessage}" id="successMessage">
		<input type="hidden" name="shoporderFormMap.shoporder_type" value="${shoporderFormMap.shoporder_type }">
		<input type="hidden" value="normal" id="shoporder_type">
    </div>

	<div id="pageNoticeInformation" class=iemes_style_NoticeDiv>
		 <label class="mds_message_label"></label>
		 <span class="iemes_style_NoticeButton"> <input type="button" submit="N" onclick="closeNoticeMessage();" value="关闭"> </span>
	</div>

	<div class="inputForm_query">
		<div class="formField_query">
			<label class="must">站点:</label> <span class="formText_query">${site}</span>
		</div>
		<div class="formField_query">
			<label class="must">工单编号:</label>
			<input type="text"
			    dataValue="shoporder_no"
				viewTitle="工单"
				data-url="/popup/queryShoporderByType.shtml"  filterId="shoporder_type"
				class="formText_query majuscule" id="tbx_shoporder_no" name="shoporderFormMap.shoporder_no" value="${shoporderFormMap.shoporder_no}"
				validate_allowedempty="N" validate_errormsg="请输入工单编号！" />
		     <input type="button" submit="N" value="检索" onclick="operationBrowse(this)" textFieldId="tbx_shoporder_no">		
		</div>
	</div>


	<!--  Tab -->
	<ul id="tabs" class="tabs_li_5tabs">
		<li><a href="#" tabid="tab1">基础信息</a></li>
		<li><a href="#" tabid="tab2">生产信息</a></li>
		<li><a href="#" tabid="tab3">工单状态调整</a></li>
		<li><a href="#" tabid="tab4">sfc列表</a></li>
		<li><a href="#" tabid="tab5">自定义数据</a></li>
	</ul>
	<div id="content">
		<div id="tab1">
		
			<div class="inputForm_content">
				<div class="formField_content">
					<label>状态:</label>  
					<c:forEach var="data" items="${shoporderStatusList}">
						    <c:if test="${shoporderFormMap.status==data.key}"><span class="formText_content">${data.value}</span></c:if>
					</c:forEach>					
				</div>
				<div class="formField_content">
				    <label class="must">物料:</label> 
					<input type="text"
					       class="formText_query majuscule"
				           dataValue="item_no" 
				           relationId= "tbx_item_id"
				           viewTitle="物料"
				           data-url="/popup/queryAllItem.shtml" 
				           validate_allowedempty='N' validate_errormsg='物料编号不能为空！'
				           id="tbx_item_name" value="${itemFormMap.item_name }"/>
					<input type="hidden" id="tbx_item_id" relationId= "tbx_item_version" dataValue="id"
					       name="shoporderFormMap.shoporder_item_id"  value="${itemFormMap.id }" />
					<input type="text" class="form_version majuscule" placeholder="版本" id="tbx_item_version"
					       dataValue="item_version" value="${itemFormMap.item_version }">
					<input type="button" submit="N" value="检索" onclick="operationBrowse(this)" textFieldId="tbx_item_name" />
				</div>
				<div class="formField_content">
					<label class="must">工艺路线:</label>
					<input type="text"
					       class="formText_query majuscule"
				           dataValue="process_workflow" 
				           relationId= "tbx_shoporder_process_workflow_id"
				           viewTitle="工艺路线"
				           data-url="/popup/queryAllProcessWorkflow.shtml" 
				           validate_allowedempty='N' validate_errormsg='工艺路线不能为空！'
				           id="tbx_process_workflow_name" value="${processWorkFlowFormMap.process_workflow }"/>
					<input type="hidden" id="tbx_shoporder_process_workflow_id" relationId= "tbx_process_workflow_version" dataValue="id"
					       name="shoporderFormMap.process_workflow_id"  value="${processWorkFlowFormMap.id }" />
					<input type="text" class="form_version majuscule" placeholder="版本" id="tbx_process_workflow_version" 
					       dataValue="process_workflow_version" value="${processWorkFlowFormMap.process_workflow_version }">
					<input type="button" submit="N" value="检索" onclick="operationBrowse(this)" textFieldId="tbx_process_workflow_name" />
				</div>
				<div class="formField_content">
					<label class="must">生产数量:</label> <input type="text"
					name="shoporderFormMap.shoporder_number" value="${shoporderFormMap.shoporder_number}"
					validate_allowedempty="N" validate_datatype = "number_int" validate_errormsg="请输入生产数量(整数)！"
						class="formText_content" id="tbx_shop_order_productionnumber" />
				</div>
				<div class="formField_content">
					<label>计划开始时间:</label> <input type="text"
					datetime_format_type="datetime"
					name="shoporderFormMap.shoporder_plan_start_time" value="${shoporderFormMap.shoporder_plan_start_time}"
						class="formText_content" id="tbx_shop_order_plan_start_time" />
				</div>
				<div class="formField_content">
					<label>计划结束时间:</label> <input type="text"
					datetime_format_type="datetime"
					name="shoporderFormMap.shoporder_plan_end_time" value="${shoporderFormMap.shoporder_plan_end_time}"
						class="formText_content" id="tbx_shop_order_plan_end_time" />
				</div>
				<div class="formField_content" >
					<label>是否自动生成sfc:</label>
					<select class="formText_content" id="select_autosfc" name="shoporderFormMap.is_sfc_auto_generate" value="${shoporderFormMap.is_sfc_auto_generate}">
                    <c:if test="${shoporderFormMap==null}">
						<option value="1" selected="selected">是</option>
						<option value="-1">否</option>
					</c:if>
					<c:if test="${shoporderFormMap.is_sfc_auto_generate==1}">
						<option value="1" selected="selected">是</option>
						<option value="-1">否</option>
					</c:if>
					<c:if test="${shoporderFormMap.is_sfc_auto_generate==-1}">
						<option value="1">是</option>
						<option value="-1" selected="selected">否</option>
					</c:if>
                    </select>
				</div>
				<div class="formField_content">
					<label>创建人:</label> <span class="formText_content">${shoporderFormMap.create_user}</span>
				</div>
				<div class="formField_content">
					<label>创建时间:</label> <span class="formText_content">${shoporderFormMap.create_time}</span>
				</div>	
			</div>
		</div>
		<div id="tab2">
			<div class="inputForm_content">				
			    <%-- <div class="formField_content">
					<label>物料:</label> <span class="formText_content">${itemFormMap.item_name}</span>
				</div>
				<div class="formField_content">
					<label>物料版本:</label> <span class="formText_content">${itemFormMap.item_version}</span>
				</div> --%>
				<div class="formField_content">
					<label>物料清单:</label> <span class="formText_content">${itemBomFormMap.item_bom_name}</span>
				</div>
				<div class="formField_content">
					<label>物料清单版本:</label> <span class="formText_content">${itemBomFormMap.item_bom_version}</span>
				</div>
				<%-- <div class="formField_content">
					<label>工艺路线:</label> <span class="formText_content">${processWorkFlowFormMap.process_workflow}</span>
				</div>	
				<div class="formField_content">
				    <label>工艺路线版本:</label> <span class="formText_content">${processWorkFlowFormMap.process_workflow_version}</span>
				</div> --%>
				<div class="formField_content">
					<label>已下达数量:</label> <span class="formText_content">${shoporderFormMap.shoporder_issued_number}</span>
				</div>
				<div class="formField_content">
					<label>完成数量:</label> <span class="formText_content">${shoporderFormMap.completed_number}</span>
				</div>
				<div class="formField_content">
					<label>报废数量:</label> <span class="formText_content">${shoporderFormMap.scrap_number}</span>
				</div>
				<div class="formField_content">
					<label>实际开始时间:</label> <span class="formText_content">${shoporderFormMap.shoporder_actual_start_time}</span>
				</div>	
				<div class="formField_content">
					<label>实际结束时间:</label> <span class="formText_content">${shoporderFormMap.shoporder_actual_end_time}</span>
				</div>	
		    </div>
		</div>
		
		<div id="tab3">
			<div class="inputForm_content">
			   <div class="formField_content">
					<label>新工单状态:</label> 
					<select class="formText_content" id="select_shoporder_status"  name="shoporderFormMap.status">
						<c:forEach var="data" items="${shoporderStatusList}">
						    <c:if test="${shoporderFormMap.status==data.key}"><option selected="selected" value="${data.key}">${data.value}</option></c:if>
							<c:if test="${shoporderFormMap.status!=data.key}"><option value="${data.key}">${data.value}</option></c:if>
						</c:forEach>
				    </select>
		        </div>
		        <div class="formField_content">
						<label>状态调整原因:</label> <textarea type="text" class="formText_content" 
						    name="shoporderFormMap.shoporder_status_change_reason"
							id="tbx_shoporder_status_change" />${shoporderStatusChangeHisoryFormMap.shoporder_status_change_reason}
							</textarea>
				</div>				
			</div>
			<div class="bt_div">
					<a class="bt_a" id="tb_add">状态调整历史</a>
			    </div>
				<table class="mtable" id="shoporder_status_change_history">
						<tr>
							<th>调整时间</th>
							<th>调整人员</th>
							<th>调整前工单状态</th>
							<th>调整后工单状态</th>
							<th>调整原因</th>						
						</tr>
						<c:forEach var="data" items="${shoporderStatusChangeHistoryList}">
							<tr>
								<td>${data.shoporder_status_change_time}</td>
								<td>${data.shoporder_status_change_user}</td>
								<td>
								    <c:forEach var="so_status" items="${shopOrderHistoryStatusList}">
							          <c:if test="${data.shoporder_status_before_change==so_status.key}">${so_status.value}</c:if>
						            </c:forEach>
								</td>
								<td>
								    <c:forEach var="so_status" items="${shopOrderHistoryStatusList}">
							          <c:if test="${data.shoporder_status_after_change==so_status.key}">${so_status.value}</c:if>
						            </c:forEach>
								</td>
								<td>${data.shoporder_status_change_reason}</td>
							</tr>
						</c:forEach>
				 </table>
			
		</div>
		
		<div id="tab4">
			<table class="mtable" id="shoporder_sfc_list">
						<tr>
							<th>sfc</th>
							<th>sfc状态</th>
							<th>当前所在操作</th>
							<th>车间</th>
							<th>车间版本</th>
							<th>产线</th>
							<th>产线版本</th>
							<th>完成时间</th>	
							<th>创建人</th>
							<th>创建时间</th>					
						</tr>
						<c:forEach var="data" items="${shoporderSfcList}">
							<tr>
								<td>${data.sfc}</td>
								<td>
								    <c:forEach var="sfc_status" items="${sfcStatusList}">
							          <c:if test="${data.sfc_status==sfc_status.key}">${sfc_status.value}</c:if>
						            </c:forEach>
								</td>
								<td>${data.operation_no}</td>
								<td>${data.workshop_name}</td>
								<td>${data.workshop_version}</td>
								<td>${data.workline_name}</td>
								<td>${data.workline_version}</td>
								<td>${data.sfc_finish_time}</td>
								<td>${data.create_user}</td>
								<td>${data.create_time}</td>
							</tr>
						</c:forEach>
		    </table>
		</div>
		<div id="tab5">
			<table class="mtable" id="uDData">
					<tr>
						<th data-value="key">数据字段</th>
						<th data-value="value">值</th>
					</tr>
					<c:forEach var="key" items="${dataKeys}">
						<tr>
							<td data-value="${key.id}">${key.data_label}</td>
							<td data-value="${key.value}">
							   <input type='text' value="${key.value}">
							</td>
						</tr>
					</c:forEach>
			    </table>
		</div>
	</div>

	<!--  Tab -->
	</form>
</body>
</html>