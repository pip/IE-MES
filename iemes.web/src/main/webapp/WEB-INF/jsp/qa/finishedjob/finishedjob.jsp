<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
<%@include file="/js/common/common.jspf"%>
<script type="text/javascript" src="js/common/form_validate.js"></script>
<script type="text/javascript" src="js/common/jtab.js"></script>
<script type="text/javascript" src="js/common/mtable.js"></script>

</head>
<body>
	<form class="mds_tab_form">
		<div class="hidden">
			<input type="hidden" name="formMap.reportMethod" value="getProductionRecordList"> 
			<input type="hidden" value="${errorMessage}" id="errorMessage"> 
			<input type="hidden" value="${successMessage}" id="successMessage">
		</div>

		<div id="pageNoticeInformation" class=iemes_style_NoticeDiv>
			<label class="mds_message_label"></label> <span
				class="iemes_style_NoticeButton"> <input type="button"
				submit="N" onclick="closeNoticeMessage();" value="关闭">
			</span>
		</div>
	
		<div id="commanButtonsDiv" class="iemes_style_commanButtonsDiv">
		    <input type="button" value="查询">
			<input type="reset" value="清空">
		</div>
		
		<div class="mds_container">
			<div class="mds_row-20"></div>
			<div class="mds_row content_search_button">
				<div class="col-md-3 col-sm-3">
					<label>送测日期</label> 
					<input type="text" id="input_begin_time" name="formMap.begin_time" style="width: 140px; margin-right: 40px">
				</div>
				<div class="col-md-3 col-sm-3">
					<label>机型</label> 
					<input type="text" id="input_begin_time" name="formMap.begin_time" style="width: 140px; margin-right: 40px">
				</div>
				<div class="col-md-3 col-sm-3">
					<label>品名</label> 
					<input type="text" id="input_begin_time" name="formMap.begin_time" style="width: 140px; margin-right: 40px">
				</div>
				<div class="col-md-3 col-sm-3">
					<label>模号</label> 
					<input type="text" id="input_begin_time" name="formMap.begin_time" style="width: 140px; margin-right: 40px">
				</div>
			</div>
			<div class="mds_row content_search_button">
				<div class="col-md-3 col-sm-3">
					<label>颜色</label> 
					<input type="text" id="input_begin_time" name="formMap.begin_time" style="width: 140px; margin-right: 40px">
				</div>
				<div class="col-md-3 col-sm-3">
					<label>送测部门</label> 
					<input type="text" id="input_begin_time" name="formMap.begin_time" style="width: 140px; margin-right: 40px">
				</div>
				<div class="col-md-3 col-sm-3">
					<label>送测人</label> 
					<input type="text" id="input_begin_time" name="formMap.begin_time" style="width: 140px; margin-right: 40px">
				</div>
				<div class="col-md-3 col-sm-3">
					<label>送测数量</label> 
					<input type="text" id="input_begin_time" name="formMap.begin_time" style="width: 140px; margin-right: 40px">
				</div>
			</div>
		</div>
		<div class="mds_row-20"></div>
		<div id="production_record_report">
			<div class="openPopup_title">全部任务列表</div>
			<table class="mtable" id="production_record_table">
				<tr>
					<th data-value="shoporder_id" class="mtable_head_hide"></th>
					<th data-value="shoporder_no">序号</th>
					<th data-value="sfc">送测日期</th>
					<th data-value="sfc_status">机型</th>
					<th data-value="main_item">品名</th>
					<th data-value="process_flow_no">模号</th>
					<th data-value="shoporder_status">颜色</th>
					<th data-value="workshop_no">送测部门</th>
					<th data-value="workline_no">送测人</th>
					<th data-value="shoporder_create_time">送测数量</th>
					<th data-value="shoporder_create_user">CPK</th>
					<th data-value="more_info">FAI</th>
					<th data-value="more_info">其它</th>
					<th data-value="more_info">测量数量</th>
					<th data-value="more_info">测量总量 </th>
					<th data-value="more_info">测量员</th>
					<th data-value="more_info">备注</th>
					<th data-value="more_info">状态</th>
				</tr>
				<tr>
					<td>1</td>
					<td>12月1日</td>
					<td>W5</td>
					<td>底壳</td>
					<td></td>
					<td>黑</td>
					<td>IQC</td>
					<td>杨镰优</td>
					<td>5</td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
					<td>谢桂丽</td>
					<td></td>
					<td>已完成</td>
				</tr>
				<tr>
					<td>2</td>
					<td>12月1日</td>
					<td>W5</td>
					<td>底壳</td>
					<td></td>
					<td>黑</td>
					<td>IQC</td>
					<td>杨镰优</td>
					<td>5</td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
					<td>谢桂丽</td>
					<td></td>
					<td>已完成</td>
				</tr>
				<tr>
					<td>3</td>
					<td>12月1日</td>
					<td>W5</td>
					<td>底壳</td>
					<td></td>
					<td>黑</td>
					<td>IQC</td>
					<td>杨镰优</td>
					<td>5</td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
					<td>谢桂丽</td>
					<td></td>
					<td>已完成</td>
				</tr>
			</table>
		</div>
	</form>

</body>
</html>