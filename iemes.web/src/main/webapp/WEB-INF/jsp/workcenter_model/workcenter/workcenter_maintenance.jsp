<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>	
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>工作中心维护</title>

<script type="text/javascript" src="js/common/form_validate.js"></script>
<script type="text/javascript" src="js/common/jtab.js"></script>
<script type="text/javascript" src="js/common/mtable.js"></script>

<script type="text/javascript" src="js/workcenter_model/workcenter/workcenter_maintenance.js"></script>

</head>
<body>
<form class="mds_tab_form">
	<div id="commanButtonsDiv" class=iemes_style_commanButtonsDiv>
			<c:forEach var="resRow" items="${res}">
				<c:if test="${resRow.type==2 && resRow.is_hide!=1}">
					<c:choose>
						<c:when test="${resRow.res_url=='btnSave'}">
							<input type="submit" id="btnSave" value="${resRow.res_name}">
						</c:when>
						<c:otherwise>
							<input type="button" id="${resRow.res_url}"
								value="${resRow.res_name}">
						</c:otherwise>
					</c:choose>
				</c:if>
			</c:forEach>
	</div>
	
	<div class="hidden">
		<input type="hidden" value="${page_res_id}" id="page_res_id">
		<input type="hidden" name="workCenterFormMap.id" value="${workcenter.id}" id="workcenter_id">
		<input type="hidden" value="${errorMessage}" id="errorMessage">
		<input type="hidden" value="${successMessage}" id="successMessage">
	</div>

	<div id="pageNoticeInformation" class=iemes_style_NoticeDiv>
		 <label class="mds_message_label"></label>
		 <span class="iemes_style_NoticeButton"> <input type="button" submit="N" onclick="closeNoticeMessage();" value="关闭"> </span>
	</div>

	<div class="inputForm_query">
		<div class="formField_query">
			<label class="must">站点:</label> <span class="formText_query">${site}</span>
		</div>
		<div class="formField_query">
			<label class="must">工作中心编号:</label> 
				<input type="text"
				dataValue="workcenter_no" 
				relationId= "tbx_version"
				viewTitle="工作中心"
				data-url="/popup/queryAllWorkCenter.shtml" 
				class="formText_query majuscule" id="tbx_workcenter_code" name="workCenterFormMap.workcenter_no" 
				validate_allowedempty="N" validate_errormsg="工作中心编号不能为空！" value="${workcenter.workcenter_no }"/>
			<input type="button" submit="N" value="检索" onclick="operationBrowse(this)" textFieldId="tbx_workcenter_code">
		</div>
	</div>


	<!--  Tab -->
	<ul id="tabs" class="tabs_li_2tabs">
		<li><a href="#" tabid="tab1">基础信息</a></li>
		<li><a href="#" tabid="tab2">自定义数据</a></li>
	</ul>
	<div id="content">
		<div id="tab1">
			<div class="inputForm_content">
				<div class="formField_content">
					<label class="must">工作中心名称:</label> <input type="text"
						class="formText_content" id="tbx_work_name" name="workCenterFormMap.workcenter_name"
						validate_allowedempty="N" validate_errormsg="工作中心名称不能为空！" value="${workcenter.workcenter_name }"/>
				</div>
				<div class="formField_content">
					<label>工作中心描述:</label> <input type="text"
						class="formText_content" id="tbx_workcenter_des" name="workCenterFormMap.workcenter_description" value="${workcenter.workcenter_description }"/>
				</div>
				<div class="formField_content">
					<label class="must">版本:</label>
					<input type="text" class="formText_content majuscule" 
					id="tbx_version" name="workCenterFormMap.workcenter_version" value="${workcenter.workcenter_version }"
					dataValue="workcenter_version"
					validate_allowedempty="N" validate_errormsg="工作中心版本号必须输入一位字符！" maxlength="1" />
				</div>
				<div class="formField_content">
					<label class="must">工作中心层级:</label> 
					<select class="formText_content"  id="select_workcenter_level" name="workCenterFormMap.workcenter_level">
						<c:if test="${workcenter.workcenter_level=='workcenter' }">
							<option value="workcenter" selected="selected">车间</option>
	                    	<option value="workline">生产线</option>
						</c:if>
						<c:if test="${workcenter.workcenter_level=='workline' }">
							<option value="workcenter">车间</option>
	                    	<option value="workline" selected="selected">生产线</option>
						</c:if>
						<c:if test="${workcenter==null }">
							<option value="workcenter" selected="selected">车间</option>
	                    	<option value="workline" >生产线</option>
						</c:if>
                    </select>
				</div>
				<div class="formField_content">
					<label class="must">工作中心类型:</label> 
					<select class="formText_content"  id="select_workcenter_type" name="workCenterFormMap.workcenter_type">
						<c:if test="${workcenter==null}">
							<option value="assemble" selected="selected">装配</option>
		                    <option value="control">调度</option>
		                    <option value="shipment">装运</option>
		                    <option value="shift">转移</option>
						</c:if>
						<c:if test="${workcenter.workcenter_type=='assemble' }">
							<option value="assemble" selected="selected">装配</option>
		                    <option value="control">调度</option>
		                    <option value="shipment">装运</option>
		                    <option value="shift">转移</option>
						</c:if>
						<c:if test="${workcenter.workcenter_type=='control' }">
							<option value="assemble">装配</option>
		                    <option value="control" selected="selected">调度</option>
		                    <option value="shipment">装运</option>
		                    <option value="shift">转移</option>
						</c:if>
						<c:if test="${workcenter.workcenter_type=='shipment' }">
							<option value="assemble">装配</option>
		                    <option value="control">调度</option>
		                    <option value="shipment" selected="selected">装运</option>
		                    <option value="shift">转移</option>
						</c:if>
						<c:if test="${workcenter.workcenter_type=='shift' }">
							<option value="assemble">装配</option>
		                    <option value="control">调度</option>
		                    <option value="shipment">装运</option>
		                    <option value="shift" selected="selected">转移</option>
						</c:if>
                    </select>
				</div>
				<div class="formField_content">
					<label>父工作中心:</label> 
					<input type="text"
						dataValue="workcenter_no" 
						viewTitle="工作中心"
						relationId="input_workcenter_id"
						data-url="/popup/queryAllWorkCenter.shtml" 
						class="formText_content majuscule" id="tbx_father_workcenter" value="${parentWorkCenter.workcenter_no}"/>
					<input type="hidden" id="input_workcenter_id" validate_errormsg="父工作中心不能为空！！！" dataValue="id" name="workCenterFormMap.workcenter_parent_id" value="${workCenterFormMap.workcenter_parent_id }">
				    <input type="button" submit="N" value="检索" onclick="operationBrowse(this)" textFieldId="tbx_father_workcenter">
				</div>
				<div class="formField_content">
					<label>是否可用:</label> 
					<select class="formText_content"  id="select_isusable" name="workCenterFormMap.status">
						<c:if test="${workcenter==null }">
							<option value="1" selected="selected">是</option>
	                    	<option value="-1">否</option>
						</c:if>
						<c:if test="${workcenter.status=='1' }">
							<option value="1" selected="selected">是</option>
	                    	<option value="-1">否</option>
						</c:if>
	                    <c:if test="${workcenter.status=='-1' }">
							<option value="1">是</option>
	                    	<option value="-1" selected="selected">否</option>
						</c:if>
                    </select>
				</div>
				<div class="formField_content">
					<label>创建人:</label> <span class="formText_content">${workCenterFormMap.create_user}</span>
				</div>
				<div class="formField_content">
					<label>创建时间:</label> <span class="formText_content">${workCenterFormMap.create_time}</span>
				</div>	
			</div>
		</div>
		<div id="tab2">
			<table class="mtable" id="uDData">
					<tr>
						<th data-value="key">数据字段</th>
						<th data-value="value">值</th>
					</tr>
					<c:forEach var="key" items="${dataKeys}">
						<tr>
							<td data-value="${key.id}">${key.data_label}</td>
							<td data-value="${key.value}">
							   <input type='text' value="${key.value}">
							</td>
						</tr>
					</c:forEach>
			</table>
		</div>
	</div>

	<!--  Tab -->
	</form>
</body>
</html>