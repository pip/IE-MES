<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<link rel="shortcut icon"
	href="${pageContext.servletContext.contextPath }/images/favicon.png"
	type="image/x-icon" />
<title>生产记录报表</title>

<%@include file="/js/common/common.jspf"%>
<script type="text/javascript" src="js/common/form_validate.js"></script>
<script type="text/javascript" src="js/common/jtab.js"></script>
<script type="text/javascript" src="js/common/mtable.js"></script>
<script type="text/javascript" src="js/report/production_record_report.js"></script>

</head>
<body>
	<form class="mds_tab_form">
		<div class="hidden">
			<input type="hidden" name="formMap.reportMethod" value="getProductionRecordList"> 
			<input type="hidden" value="${errorMessage}" id="errorMessage"> 
			<input type="hidden" value="${successMessage}" id="successMessage">
		</div>

		<div id="pageNoticeInformation" class=iemes_style_NoticeDiv>
			<label class="mds_message_label"></label> <span
				class="iemes_style_NoticeButton"> <input type="button"
				submit="N" onclick="closeNoticeMessage();" value="关闭">
			</span>
		</div>

		<div class="mds_container">
			<div class="mds_row-20"></div>
			<div class="mds_row content_search_button">
				<div class="col-md-4 col-sm-4">
					<label>工单开始时间</label> 
					<input type="text" id="input_begin_time" name="formMap.begin_time" datetime_format_type="datetime" style="margin-right: 20px;">
				</div>
				<div class="col-md-3 col-sm-3">
					<label>工单</label> 
					<input type="text" id="input_shoporder_no"
						dataValue="shoporder_no"
						viewTitle="工单"
						data-url="/popup/queryAllShoporder.shtml"
						name="formMap.shoporder_no">
					<input type="button" value="检索" onclick="operationBrowse(this)" textFieldId="input_shoporder_no">
				</div>
				<div class="col-md-3 col-sm-3">
					<label>车间</label> 
					<input type="text" id="input_workshop_no"
						dataValue="workcenter_no"
						viewTitle="车间"
						data-url="/popup/queryWorkCenter.shtml"
						name="formMap.workshop_no">
					<input type="button" value="检索" onclick="operationBrowse(this)"  textFieldId="input_workshop_no">
				</div>
				
				<div class="col-md-2 col-sm-2"></div>
			</div>
			<div class="mds_row content_search_button">
				<div class="col-md-4 col-sm-4">
					<label>工单结束时间</label> 
					<input type="text" id="input_end_time" name="formMap.end_time" datetime_format_type="datetime" style="margin-right: 20px;">
				</div>
				<div class="col-md-3 col-sm-3">
					<label>产线</label> 
					<input type="text" id="input_workline_no"
						dataValue="workcenter_no"
						viewTitle="产线"
						filterId="input_workcenter_id"
						data-url="/popup/queryWorkLine.shtml"
						name="formMap.workline_no">
					<input type="button" value="检索" onclick="operationBrowse(this)"  textFieldId="input_workline_no">
				</div>
				<div class="col-md-3 col-sm-3">
					<label>SFC</label> 
					<input type="text" id="input_sfc"
						dataValue="sfc"
						viewTitle="sfc"
						data-url="/popup/queryAllSfc.shtml"
						name="formMap.sfc">
					<input type="button" value="检索" onclick="operationBrowse(this)" textFieldId="input_sfc">
				</div>
				<div class="col-md-2 col-sm-2"></div>
			</div>
			<div class="mds_row">
				<div class="mds_row-10"></div>
				<div class="col-md-6 col-sm-6">
					<input type="submit" value="查询">
				</div>
				<div class="col-md-1 col-sm-1 text_center">
					<input type="reset" value="清空">
				</div>
			</div>
		</div>
		<div class="mds_row-20"></div>
		<div id="production_record_report">
			<div class="openPopup_title">SFC生产记录表</div>
			<table class="mtable" id="production_record_table">
				<tr>
					<th data-value="shoporder_id" class="mtable_head_hide"></th>
					<th data-value="shoporder_no">工单</th>
					<th data-value="sfc">SFC</th>
					<th data-value="sfc_status">SFC状态</th>
					<th data-value="main_item">成品物料</th>
					<th data-value="process_flow_no">工艺路线</th>
					<th data-value="shoporder_status">工单状态</th>
					<th data-value="workshop_no">车间</th>
					<th data-value="workline_no">产线</th>
					<th data-value="shoporder_create_time">工单创建时间</th>
					<th data-value="shoporder_create_user">创建人</th>
					<th data-value="more_info">详细信息</th>
				</tr>
			</table>
		</div>
	</form>
</body>
</html>