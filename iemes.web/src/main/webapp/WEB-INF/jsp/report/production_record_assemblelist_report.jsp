<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@include file="/js/common/common.jspf"%>
<c:set var="ctx" value="${pageContext.request.contextPath}" />
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<link rel="shortcut icon"
	href="${pageContext.servletContext.contextPath }/images/favicon.png"
	type="image/x-icon" />
<title>SFC装配信息列表</title>

<script type="text/javascript" src="${ctx }/js/common/form_validate.js"></script>
<script type="text/javascript" src="${ctx }/js/common/jtab.js"></script>
<script type="text/javascript" src="${ctx }/js/common/mtable.js"></script>

</head>
<body>
	<form class="mds_tab_form">
		<div class="hidden">
			<input type="hidden" name="formMap.reportMethod" value="getProductionRecordList"> 
			<input type="hidden" value="${errorMessage}" id="errorMessage"> 
			<input type="hidden" value="${successMessage}" id="successMessage">
		</div>

		<div id="pageNoticeInformation" class=iemes_style_NoticeDiv>
			<label class="mds_message_label"></label> <span
				class="iemes_style_NoticeButton"> <input type="button"
				submit="N" onclick="closeNoticeMessage();" value="关闭">
			</span>
		</div>

		<div id="production_record_report">
			<div class="openPopup_title">SFC装配信息列表</div>
			<table class="mtable" id="production_record_flowstep_table">
				<tr>
					<th data-value="sfc">产品SFC</th>
					<th data-value="workshop_no">车间</th>
					<th data-value="workline_no">产线</th>
					<th data-value="operation_no">操作</th>
					<th data-value="work_resource_no">资源</th>
					<th data-value="assemble_item">装配物料</th>
					<th data-value="assemble_sfc">装配物料SFC</th>
					<th data-value="assemble_num">装配数量</th>
					<th data-value="step_create_time">操作开始时间</th>
					<th data-value="step_create_user">创建人</th>
				</tr>
				<c:forEach var="data" items="${list }">
					<tr>
						<td>${data.sfc }</td>
						<td>${data.workshop_no }</td>
						<td>${data.workline_no }</td>
						<td>${data.operation_no }</td>
						<td>${data.work_resource_no }</td>
						<td>${data.assemble_item }</td>
						<td>${data.assemble_sfc }</td>
						<td>${data.assemble_num }</td>
						<td>${data.step_create_time }</td>
						<td>${data.step_create_user }</td>
					</tr>
				</c:forEach>
			</table>
		</div>
	</form>
</body>
</html>