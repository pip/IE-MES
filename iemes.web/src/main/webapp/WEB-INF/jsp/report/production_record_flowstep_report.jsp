<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@include file="/js/common/common.jspf"%>
<c:set var="ctx" value="${pageContext.request.contextPath}" />
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<link rel="shortcut icon"
	href="${pageContext.servletContext.contextPath }/images/favicon.png"
	type="image/x-icon" />
<title>生产步骤信息列表</title>

<script type="text/javascript" src="${ctx }/js/common/form_validate.js"></script>
<script type="text/javascript" src="${ctx }/js/common/jtab.js"></script>
<script type="text/javascript" src="${ctx }/js/common/mtable.js"></script>
<script type="text/javascript"
	src="${ctx }/js/report/production_record_flowstep_report.js"></script>

</head>
<body>
	<form class="mds_tab_form">
		<div class="hidden">
			<input type="hidden" name="formMap.reportMethod"
				value="getProductionRecordList"> <input type="hidden"
				value="${errorMessage}" id="errorMessage"> <input
				type="hidden" value="${successMessage}" id="successMessage">
		</div>

		<div id="pageNoticeInformation" class=iemes_style_NoticeDiv>
			<label class="mds_message_label"></label> <span
				class="iemes_style_NoticeButton"> <input type="button"
				submit="N" onclick="closeNoticeMessage();" value="关闭">
			</span>
		</div>

		<!--  Tab -->
		<ul id="tabs" class="tabs_li_2tabs">
			<li><a href="#" tabid="tab1">SFC工艺步骤列表</a></li>
			<li><a href="#" tabid="tab2">SFC生产步骤列表</a></li>
		</ul>
		<div id="content">
			<div id="tab1">
				<div id="production_record_report">
					<table class="mtable" id="production_record_flowstep_table">
						<tr>
							<th data-value="step_level">步骤顺序</th>
							<th data-value="process_workflow">工艺路线</th>
							<th data-value="operation_no">操作</th>
							<th data-value="sfc">SFC</th>
							<th data-value="main_item">成品物料</th>
							<th data-value="step_status">状态</th>
							<th data-value="workshop_no">车间</th>
							<th data-value="workline_no">产线</th>
							<th data-value="step_create_time">操作开始时间</th>
							<th data-value="step_create_user">创建人</th>
							<th data-value="assemble_info">装配信息</th>
							<th data-value="nc_info">不良信息</th>
						</tr>
						<c:forEach var="data" items="${list }">
							<tr>
								<td>${data.step_level }</td>
								<td>${data.process_workflow }</td>
								<td>${data.operation_no }</td>
								<td>${data.sfc }</td>
								<td>${data.main_item }</td>
								<td>${data.step_status }</td>
								<td>${data.workshop_no }</td>
								<td>${data.workline_no }</td>
								<td>${data.step_create_time }</td>
								<td>${data.step_create_user }</td>
								<td><a class="flow_assemble_info" href="#">${data.assemble_info }</a></td>
								<td><a class="flow_nc_info" href="#">${data.nc_info }</td>
							</tr>
						</c:forEach>
					</table>
				</div>
			</div>
			<div id="tab2">
				<div id="production_record_sfcstep_report">
					<table class="mtable" id="production_record_sfcstep_table">
						<tr>
							<th data-value="sfc">SFC</th>
							<th data-value="workshop_no">车间</th>
							<th data-value="workline_no">产线</th>
							<th data-value="work_resource_no">资源</th>
							<th data-value="operation_no">操作</th>
							<th data-value="step_status">状态</th>
							<th data-value="step_create_time">操作开始时间</th>
							<th data-value="step_">创建人</th>
							<th data-value="step_remark">备注</th>
							<th data-value="assemble_info">装配信息</th>
							<th data-value="nc_info">不良信息</th>
						</tr>
						<c:forEach var="data" items="${list2 }">
							<tr>
								<td>${data.sfc }</td>
								<td>${data.workshop_no }</td>
								<td>${data.workline_no }</td>
								<td>${data.work_resource_no }</td>
								<td>${data.operation_no }</td>
								<td>${data.step_status }</td>
								<td>${data.step_create_time }</td>
								<td>${data.step_create_user }</td>
								<td>${data.step_remark }</td>
								<td><a class="step_assemble_info" href="#">${data.assemble_info }</a></td>
								<td><a class="step_nc_info" href="#">${data.nc_info }</td>
							</tr>
						</c:forEach>
					</table>
				</div>
			</div>
		</div>
	</form>
</body>
</html>