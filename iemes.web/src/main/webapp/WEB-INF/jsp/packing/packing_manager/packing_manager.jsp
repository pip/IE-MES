<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>	
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>包装管理</title>

<script type="text/javascript" src="js/common/form_validate.js"></script>
<script type="text/javascript" src="js/common/jtab.js"></script>
<script type="text/javascript" src="js/common/mtable.js"></script>
<script type="text/javascript"
	src="js/packing/packing_manager/packing_maintenance.js"></script>

</head>
<body>
	<form class="mds_tab_form">
		<div id="commanButtonsDiv" class=iemes_style_commanButtonsDiv>
			<input type="button" id="btnQuery" value="检索"> 
			<input type="button" id="btnClear" value="清除"> 
			<input type="button" id="btnOpen" value="解包"> 
			<input type="button" id="btnClose" value="打包">
			<input type="button" id="generateCode" value="生成新条码">
		</div>
		<div class="hidden">
			<input type="hidden" name="containerTypeFormMap.number_rule_id" 
				value="${containerTypeFormMap.number_rule_id}" id="number_rule_id" dataValue="number_rule_id">
			<input type="hidden" value="${errorMessage}" id="errorMessage">
			<input type="hidden" value="${successMessage}" id="successMessage">
			<input type="hidden" name="containerFormMap.container_status"
				value="${containerFormMap.container_status}" id="container_status">
		</div>
		<div id="pageNoticeInformation" class=iemes_style_NoticeDiv>
			<label class="mds_message_label"></label> <span
				class="iemes_style_NoticeButton"> <input type="button"
				submit="N" onclick="closeNoticeMessage();" value="关闭">
			</span>
		</div>

		<div class="inputForm_query">
			<div class="formField_query">
				<label class="must">站点:</label> <span class="formText_query">${site }</span>
			</div>
			<div class="formField_query">
				<label class="must">容器类型:</label>
				<input type="text"
					class="formText_query" dataValue="container_type_no" viewTitle="容器类型"
					relationId="tbx_container_type_id"
					data-url="/popup/queryAvailableContainer.shtml"
					callBackFun="chooseContainerType"
					id="tbx_container_type_no" name="containerTypeFormMap.container_type_no"
					value="${containerTypeFormMap.container_type_no }" 
					validate_allowedempty="N" validate_errormsg="请输入容器类型编号！" />
				<input type="hidden" id="tbx_container_type_id" dataValue="id" relationId="number_rule_id"
					 name="ContainerTypeFormMap.id" value="${containerTypeFormMap.id}">	
				<input type="button" submit="N" value="检索" onclick="operationBrowse(this)" textFieldId="tbx_container_type_no">
			</div>
			<div>
				<label>容器条码:</label> 
				<input type="text" class="formText_query" value="${container_id }"
					id="input_container_id" name="containerTypeFormMap.container_id"
					validate_allowedempty="N" validate_errormsg="请输入容器条码！"/> 
			</div>
		</div>

		<!--  Tab -->
		<ul id="tabs" class="tabs_li_1tabs">
			<li><a href="#" tabid="tab1">包装信息</a></li>
		</ul>
		<div class="iemes_style_packtable">
			<div class="iemes_style_packing">
				<div class="inputForm_content2">

					<div class="formField_content">
						<label>容器数据:</label> <span class="formText_content">长:${containerTypeFormMap.container_length }mm;宽:${containerTypeFormMap.container_width }mm;高:${containerTypeFormMap.container_height }mm;重量：${containerTypeFormMap.container_weight }</span>
					</div>
					<div class="formField_content">
						<label>最小数量:</label> <span class="formText_content">${containerTypeFormMap.min_number }</span>
					</div>
					<div class="formField_content">
						<label>最大数量:</label> <span class="formText_content" id="max_num">${containerTypeFormMap.max_number }</span>
					</div>
					<div class="formField_content">
						<label>当前数量:</label> <span class="formText_content" id="Container_count">${sfcCount }</span>
					</div>

					<div class="formField_content">
						<c:if test="${containerTypeFormMap==null}">
							<label class="must" id="packingType" data-value="box">车间作业控制:</label>
							<input type="text" dataValue="sfc" viewTitle="车间作业控制" data-url="/popup/getFinishAndUnPackingSfc.shtml"
								class="formText_query majuscule" id="tbx_sfc_no" validate_errormsg="请输入车间作业控制！" /> 
						</c:if>
						<c:if test="${containerTypeFormMap.container_level=='box' }">
							<label class="must" id="packingType" data-value="box">车间作业控制:</label>
							<input type="text" dataValue="sfc" viewTitle="车间作业控制" data-url="/popup/getFinishAndUnPackingSfc.shtml"
								class="formText_query majuscule" id="tbx_sfc_no" validate_errormsg="请输入车间作业控制！" /> 
						</c:if>
						<c:if test="${containerTypeFormMap.container_level=='carton' }">
							<label class="must" id="packingType" data-value="carton">礼盒:</label>
							<input type="text" dataValue="container_id" viewTitle="礼盒" data-url="/popup/getUnPackingBox.shtml"
								class="formText_query majuscule" id="tbx_sfc_no" validate_errormsg="请输入礼盒条码！" /> 
						</c:if>
						<c:if test="${containerTypeFormMap.container_level=='pallet' }">
							<label class="must" id="packingType" data-value="pallet">中箱:</label>
							<input type="text" dataValue="container_id" viewTitle="中箱" data-url="/popup/getUnPackingCarton.shtml"
								class="formText_query majuscule" id="tbx_sfc_no" validate_errormsg="请输入中箱条码！" /> 
						</c:if>
							
						<input type="button" submit="N" value="检索" onclick="operationBrowse(this)" textFieldId="tbx_sfc_no">
					</div>
					<div>
						<label>操作:</label> <input type="button" submit="N"
							class="formText_content" id="btnAddSFC" value="添加">
					</div>
				</div>

			</div>
			<div class="iemes_style_packed">
				<table class="mtable" id="containerSfcList">
					<tr>
						<th  data-value="value" width = '700px'>物品</th>
						<th width = '107px'>操作</th>
					</tr>
					<c:forEach var="data" items = "${listContainerSFCFormMap}">
						<tr>
							<td width = '700px' data-value="${data.wrappage}" >${data.wrappage}</td>
							<td width = '107px'><a href="#" class="bt_delete_row">删除</a></td>
						</tr>
					</c:forEach>
				</table>
			</div>
		</div>
		<!--  Tab -->
	</form>
</body>
</html>