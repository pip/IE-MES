<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="utf-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>汇总报表</title>
</head>
<body>
	<form class="mds_tab_form">
		<div class="hidden">
			<input type="hidden" name="formMap.reportMethod" value="getProductionRecordList"> 
			<input type="hidden" value="${errorMessage}" id="errorMessage"> 
			<input type="hidden" value="${successMessage}" id="successMessage">
		</div>

		<div id="pageNoticeInformation" class=iemes_style_NoticeDiv>
			<label class="mds_message_label"></label> <span
				class="iemes_style_NoticeButton"> <input type="button"
				submit="N" onclick="closeNoticeMessage();" value="关闭">
			</span>
		</div>
	
		<div id="commanButtonsDiv" class="iemes_style_commanButtonsDiv">
			<input type="button" id="btnQuery" value="检索">
			<input type="button" id="btnQuery" value="清除">
		</div>
		
		<div class="mds_container">
			<div class="mds_row-20"></div>
			<div class="mds_row content_search_button">
				<div class="col-md-3 col-sm-3">
					<label>报告日期</label> 
					<input type="text" id="input_begin_time" name="formMap.begin_time" style="width: 140px; margin-right: 40px">
				</div>
				<div class="col-md-3 col-sm-3">
					<label>机型</label> 
					<input type="text" id="input_begin_time" name="formMap.begin_time" style="width: 140px; margin-right: 40px">
				</div>
				<div class="col-md-3 col-sm-3">
					<label>品名</label> 
					<input type="text" id="input_begin_time" name="formMap.begin_time" style="width: 140px; margin-right: 40px">
				</div>
				<div class="col-md-3 col-sm-3">
					<label>颜色</label> 
					<input type="text" id="input_begin_time" name="formMap.begin_time" style="width: 140px; margin-right: 40px">
				</div>
			</div>
			<div class="mds_row content_search_button">
				<div class="col-md-3 col-sm-3">
					<label>油漆供应商</label> 
					<input type="text" id="input_begin_time" name="formMap.begin_time" style="width: 140px; margin-right: 40px">
				</div>
				<div class="col-md-3 col-sm-3">
					<label>测试类别</label> 
					<input type="text" id="input_begin_time" name="formMap.begin_time" style="width: 140px; margin-right: 40px">
				</div>
				<div class="col-md-3 col-sm-3">
					<label>判定结果</label> 
					<input type="text" id="input_begin_time" name="formMap.begin_time" style="width: 140px; margin-right: 40px">
				</div>
				<div class="col-md-3 col-sm-3">
					<label>不合格项目</label> 
					<input type="text" id="input_begin_time" name="formMap.begin_time" style="width: 140px; margin-right: 40px">
				</div>
			</div>
		</div>
		<div class="mds_row-20"></div>
		<div id="production_record_report">
			<div class="openPopup_title">报告汇总表</div>
			<table class="mtable" id="production_record_table">
				<tr>
					<th data-value="shoporder_no">序号</th>
					<th data-value="sfc">报告日期</th>
					<th data-value="sfc_status">机型</th>
					<th data-value="main_item">品名</th>
					<th data-value="shoporder_status">颜色</th>
					<th data-value="workshop_no">油漆供应商</th>
					<th data-value="workline_no">测试类别</th>
					<th data-value="shoporder_create_time">判定结果</th>
					<th data-value="shoporder_create_user">不合格项目</th>
					<th data-value="more_info">不良描述</th>
					<th data-value="more_info">责任人</th>
					<th data-value="more_info">推动改善人</th>
					<th data-value="more_info">备注</th>
				</tr>
				<tr>
					<td>29</td>
					<td>2017/11/15</td>
					<td>ZQL1698</td>
					<td>后壳</td>
					<td>黑色</td>
					<td></td>
					<td>试产</td>
					<td>NG</td>
					<td>表面能测试</td>
					<td>墨水收缩实测34B</td>
					<td></td>
					<td></td>
					<td></td>
				</tr>
				<tr>
					<td rowspan="2">30</td>
					<td rowspan="2">2017/11/15</td>
					<td rowspan="2">ZQL1698</td>
					<td rowspan="2">后壳</td>
					<td rowspan="2">蓝色</td>
					<td rowspan="2">施美克</td>
					<td rowspan="2">试产</td>
					<td rowspan="2">NG</td>
					<td>拉力</td>
					<td>拉力小于3kgf</td>
					<td></td>
					<td></td>
					<td></td>
				</tr>
				<tr>
					<td>表面能测试</td>
					<td>墨水收缩实测34C</td>
					<td></td>
					<td></td>
					<td></td>
				</tr>
				<tr>
					<td>34</td>
					<td>2017/11/18</td>
					<td>ZQL1698</td>
					<td>后壳</td>
					<td>白色</td>
					<td></td>
					<td>试产</td>
					<td>OK</td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
				</tr>
			</table>
		</div>
	</form>
</body>
</html>