<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>		
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>条码打印</title>

<script type="text/javascript" src="js/common/form_validate.js"></script>
<script type="text/javascript" src="js/common/jtab.js"></script>
<script type="text/javascript" src="js/common/mtable.js"></script>
<script type="text/javascript" src="js/work_process/barcode_printing/barcode_printing.js"></script>

</head>
<body>
<form class="mds_tab_form">
	<div id="commanButtonsDiv" class=iemes_style_commanButtonsDiv>
		<input type="button" id="btnQuery"  value="检索"> 
		<input type="button" id="btnClear" value="清除"> 
		<input type="button" id="btnPrint"  value="打印全部"> 
	</div>
	
	<div class="hidden">
		<input type="hidden" value="${errorMessage}" id="errorMessage">
		<input type="hidden" value="${successMessage}" id="successMessage">
	</div>

	<div id="pageNoticeInformation" class=iemes_style_NoticeDiv>
		 <label class="mds_message_label"></label>
		 <span class="iemes_style_NoticeButton"> <input type="button" submit="N" onclick="closeNoticeMessage();" value="关闭"> </span>
	</div>

	<div class="inputForm_query">
		<div class="formField_query">
			<label class="must">站点:</label> <span class="formText_query">${site }</span>
		</div>
		<div class="formField_query">
			<label class="must">工单:</label> 
			<input type="text"
				dataValue="shoporder_no"
				viewTitle="工单"
				data-url="/popup/queryAllShoporder.shtml" 
				class="formText_query" id="tbx_shop_order_no" value="${shoporderFormMap.shoporder_no }"/>
			<input type="button" submit="N" value="检索" onclick="operationBrowse(this)" textFieldId="tbx_shop_order_no">
		</div>
		<div class="formField_query">
				<label>车间:</label> 
				<input type="text"
					dataValue="workcenter_no"
					viewTitle="车间"
					relationId="input_workcenter_id"
					data-url="/popup/queryWorkCenter.shtml" 
					class="formText_query" id="tbx_workcenter_no" value="${shopOrderSfcFormMap.workcenter }"/>
				<input type="hidden" id="input_workcenter_id" validate_errormsg="车间不能为空！" relationId= "tbx_workcenter_version"
				       dataValue="id" value="${shoporderFormMap.workcenter_id }">	
				<input type="text" class="form_version majuscule" placeholder="版本" id="tbx_workcenter_version"
				       dataValue="workcenter_version" value="${shopOrderSfcFormMap.workcenter_version }">
				<input type="button" submit="N" value="检索" onclick="operationBrowse(this)" textFieldId="tbx_workcenter_no">
		</div>
	    <div class="formField_query">
				<label>产线:</label> 
				<input type="text"
					dataValue="workcenter_no"
					filterId="input_workcenter_id"
					relationId="input_workline_id"
					viewTitle="产线"
					data-url="/popup/queryWorkLine.shtml" 
					class="formText_query" id="tbx_workline_no" value="${shopOrderSfcFormMap.workline }"/>
				<input type="hidden" id="input_workline_id" validate_errormsg="产线不能为空！" relationId= "tbx_workline_version"
				       dataValue="id" value="${shoporderFormMap.workline_id }">	
				<input type="text" class="form_version majuscule" placeholder="版本" id="tbx_workline_version"
				       dataValue="workcenter_version" value="${shopOrderSfcFormMap.workline_version }">
				<input type="button" submit="N" value="检索" onclick="operationBrowse(this)" textFieldId="tbx_workline_no">
		 </div>
    </div> 


	<!--  Tab -->
	<ul id="tabs" class="tabs_li_1tabs">
		<li><a href="#" tabid="tab1">工单SFC列表</a></li>
	</ul>
	<div id="content">
		<div id="tab1">
			<table class="mtable" id="barcode_table">
				<tr>
					<th data-value="shoporder_no">工单编号</th>
					<th data-value="sfc">SFC</th>
					<th data-value="sfc_status">SFC状态</th>
					<th data-value="create_user">创建人</th>
					<th data-value="create_user">打印状态</th>
				</tr>
				<c:forEach var="shopOrderSfcFormMap" items="${shopOrderSfcFormMapList }">
					<tr>
						<td >${shoporderFormMap.shoporder_no }</td>
						<td >${shopOrderSfcFormMap.sfc }</td>
						
						<c:if test="${shopOrderSfcFormMap.sfc_status==0 }"><td >创建</td></c:if>
						<c:if test="${shopOrderSfcFormMap.sfc_status==1 }"><td >已下达</td></c:if>
						<c:if test="${shopOrderSfcFormMap.sfc_status==2 }"><td >生产中</td></c:if>
						<c:if test="${shopOrderSfcFormMap.sfc_status==3 }"><td >已完成</td></c:if>
						<c:if test="${shopOrderSfcFormMap.sfc_status==4 }"><td >关闭</td></c:if>
						<c:if test="${shopOrderSfcFormMap.sfc_status==5 }"><td >扶起</td></c:if>
						<c:if test="${shopOrderSfcFormMap.sfc_status==6 }"><td >已删除</td></c:if>
						
						<td >${shopOrderSfcFormMap.create_user }</td>
						<c:if test="${shopOrderSfcFormMap.is_printed==1 }"><td >已打印</td></c:if>
						<c:if test="${shopOrderSfcFormMap.is_printed==-1 }"><td >未打印</td></c:if>
					</tr>
				</c:forEach>
			</table>
		</div>
	</div>

	<!--  Tab -->
	</form>
</body>
</html>