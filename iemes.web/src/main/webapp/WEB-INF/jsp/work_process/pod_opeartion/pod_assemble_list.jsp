<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<%@include file="/js/common/common.jspf"%>
<script type="text/javascript" src="${ctx}/js/common/mtable.js"></script>
<script type="text/javascript" src="${ctx}/js/work_process/pod_operation/pod_assemble_list.js"></script>
<title>装配物料清单</title>
</head>
<body class="openPopup">
	<form class="mds_tab_form">
		<table class="mtable" id="assemble_item_list_table">
			<tr>
				<th class="mtable_head_hide" data-value="main_item_id">main_item_id</th>
				<th class="mtable_head_hide" data-value="shoporder_id">shoporder_id</th>
				<th class="mtable_head_hide" data-value="sfc">sfc</th>
				<th class="mtable_head_hide" data-value="item_name">item_name</th>
				<th class="mtable_head_hide" data-value="item_desc">item_desc</th>
				<th class="mtable_head_hide" data-value="item_sfc_id">物料ID</th>
				<th class="mtable_head_hide" data-value="workshop_id">车间ID</th>
				<th class="mtable_head_hide" data-value="workline_id">产线ID</th>
				<th class="mtable_head_hide" data-value="operation_id">操作ID</th>
				<th class="mtable_head_hide" data-value="work_resource_id">资源ID</th>
				<th data-value="item_no">物料</th>
				<th data-value="use_num">装配数量</th>
				<th data-value="assemble_num">已装配数量</th>
				<th data-value="workshop_no">车间</th>
				<th data-value="workline_no">产线</th>
				<th data-value="operation_no">操作</th>
				<th data-value="work_resource_no">资源</th>
				<th data-value="assemble_info">装配详情</th>
			</tr>
			<c:forEach var="data" items="${dataList}">
				<tr>
					<td class="mtable_head_hide" >${data.main_item_id}</td>
					<td class="mtable_head_hide" >${data.shoporder_id}</td>
					<td class="mtable_head_hide" >${data.sfc}</td>
					<td class="mtable_head_hide" >${data.item_name}</td>
					<td class="mtable_head_hide" >${data.item_desc}</td>
					<td class="mtable_head_hide" >${data.item_sfc_id}</td>
					<td class="mtable_head_hide" >${data.workshop_id}</td>
					<td class="mtable_head_hide" >${data.workline_id}</td>
					<td class="mtable_head_hide" >${data.operation_id}</td>
					<td class="mtable_head_hide" >${data.work_resource_id}</td>
					<td>${data.item_no}</td>
					<td>${data.use_num}</td>
					<td>${data.assemble_num}</td>
					<td>${data.workshop_no}</td>
					<td>${data.workline_no}</td>
					<td>${data.operation_no}</td>
					<td>${data.work_resource_no}</td>
					<td><a href="#">装配详情</a></td>
				</tr>
			</c:forEach>
		</table>
	</form>
</body>
</html>