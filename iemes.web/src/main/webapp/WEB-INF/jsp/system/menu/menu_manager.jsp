<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>	
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>菜单管理</title>

<script type="text/javascript" src="js/common/form_validate.js"></script>
<script type="text/javascript" src="js/common/jtab.js"></script>
<script type="text/javascript" src="js/system/menu/menu_manager.js"></script>

</head>
<body>
<form class="mds_tab_form">
	<div id="commanButtonsDiv" class=iemes_style_commanButtonsDiv>
		<input type="button" id="btnQuery" value="检索"> 
		<input type="submit" id="btnSave"  value="保存"> 
		<input type="button" id="btnClear" value="清除"> 
		<input type="button" id="btnDelete" value="删除">
	</div>
	
	<div class="hidden">
		<input type="hidden" name="resFormMap.id" value="${resFormMap.id}" id="menu_id">
		<input type="hidden" value="${errorMessage}" id="errorMessage">
		<input type="hidden" value="${successMessage}" id="successMessage">
	</div>

	<div id="pageNoticeInformation" class=iemes_style_NoticeDiv>
		 <label class="mds_message_label"></label>
		 <span class="iemes_style_NoticeButton"> <input type="button" submit="N" onclick="closeNoticeMessage();" value="关闭"> </span>
	</div>

	<div class="inputForm_query">
		<div class="formField_query">
			<label class="must">菜单标识:</label> 
			
			<input type="text"
				dataValue="res_key" 
				viewTitle="菜单"
				data-url="/popup/queryAllMenu.shtml" 
				class="formText_query majuscule" id="tbx_menu_key" name="resFormMap.res_key" value="${resFormMap.res_key }"
				validate_allowedempty="N" validate_errormsg="菜单标识不能为空！" />
			<input type="button" submit="N" value="检索"  onclick="operationBrowse(this)"  textFieldId="tbx_menu_key">
		</div>
	</div>


	<!--  Tab -->
	<ul id="tabs" class="tabs_li_1tabs">
		<li><a href="#" tabid="tab1">基础信息</a></li>
	</ul>
	<div id="content">
		<div id="tab1">
			<div class="inputForm_content">
				<div class="formField_content">
					<label class="must">菜单名称:</label> <input type="text"
						class="formText_content" id="tbx_menu_name" validate_allowedempty="N" validate_errormsg="请输入菜单名称！" 
						 name="resFormMap.res_name" value="${resFormMap.res_name}" />
				</div>
				<div class="formField_content">
					<label>菜单url:</label> 
					<textarea type="text"
						class="formText_content" id="tbx_menu_url" validate_allowedempty="N" validate_errormsg="请输入菜单url！" 
						 name="resFormMap.res_url">${resFormMap.res_url}</textarea>
				</div>
				<div class="formField_content">
					<label>上级菜单:</label>
					<div class="hidden">
		             <input type="hidden" id="menu_parent_id" value="${resFormMap.parent_id}">
	                </div>
					<select class="formText_content" id="select_menu_parent_id" name="resFormMap.parent_id" value="${resFormMap.parent_id}">
                    </select>
				</div>
				<div class="formField_content">
					<label class="must">层级:</label> <input type="text"
						class="formText_content" id="tbx_menu_name" validate_allowedempty="N" validate_datatype="number_int" validate_errormsg="请输入层级（整数）！" 
						 name="resFormMap.level" value="${resFormMap.level}" />
				</div>
				<div class="formField_content">
					<label>菜单类型:</label>
					<select class="formText_content" id="select_menu_type" name="resFormMap.type" value="${resFormMap.type}">
					<c:if test="${resFormMap==null}">
						<option value="0" selected="selected">目录</option>
						<option value="1">菜单</option>
						<option value="2">按钮</option>
					</c:if>
					<c:if test="${resFormMap.type==0}">
						<option value="0" selected="selected">目录</option>
						<option value="1">菜单</option>
						<option value="2">按钮</option>
					</c:if>
					<c:if test="${resFormMap.type==1}">
						<option value="0">目录</option>
						<option value="1" selected="selected">菜单</option>
						<option value="2">按钮</option>
					</c:if>
					<c:if test="${resFormMap.type==2}">
						<option value="0">目录</option>
						<option value="1">菜单</option>
						<option value="2" selected="selected">按钮</option>
					</c:if>
                    </select>
				</div>
				<div class="formField_content">
					<label>图标:</label> <input type="text" name="resFormMap.icon" value="${resFormMap.icon}"
						class="formText_content" id="tbx_menu_icon" />
				</div>
				<div class="formField_content" >
					<label>是否隐藏:</label>
					<select class="formText_content" id="select_menu_ishidden" name="resFormMap.is_hide" value="${resFormMap.is_hide}">
                    <c:if test="${resFormMap==null}">
						<option value="1" selected="selected">是</option>
						<option value="-1">否</option>
					</c:if>
					<c:if test="${resFormMap.is_hide==1}">
						<option value="1" selected="selected">是</option>
						<option value="-1">否</option>
					</c:if>
					<c:if test="${resFormMap.is_hide==-1}">
						<option value="1">是</option>
						<option value="-1" selected="selected">否</option>
					</c:if>
                    </select>
				</div>
				<div class="formField_content">
					<label>描述:</label> <input type="text" name="resFormMap.description" value="${resFormMap.description}"
						class="formText_content" id="tbx_menu_des" />
				</div>
				<div class="formField_content">
					<label>创建人:</label> <span class="formText_content">${resFormMap.create_user}</span>
				</div>
				<div class="formField_content">
					<label>创建时间:</label> <span class="formText_content">${resFormMap.create_time}</span>
				</div>	
			</div>
		</div>

	</div>

	<!--  Tab -->
	</form>
</body>
</html>