<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>index menu</title>
<link rel="stylesheet" href="css/index/index_menu.css" type="text/css" />
<script type="text/javascript" src="js/index/index_menu.js">
	
</script>
</head>
<body>
	<ul class="menu">
		<c:forEach var="key" items="${list}" varStatus="s">
			<li class="menu_li" isChildrenOpen="true"><a class="menu_li_one" href="#">${key.res_name}</a>
				<ul class="menu_ul">
					<c:forEach var="kc" items="${key.children}">
						<li class="menu_li2"><a class="menu_li_children" href="#" data-resources-id="${kc.id}"
						data-url="${kc.res_url}?id=${kc.id}">${kc.res_name}</a></li>
					</c:forEach>
				</ul>
			</li>
		</c:forEach>
	</ul>
</body>
</html>