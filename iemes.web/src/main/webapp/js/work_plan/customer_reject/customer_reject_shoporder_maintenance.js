$(document).ready(function() {
	
	if (!stringIsNull($('#errorMessage').val())) {
		showErrorNoticeMessage($('#errorMessage').val());
	}
	
	if (!stringIsNull($('#successMessage').val())) {
		showSuccessNoticeMessage($('#successMessage').val());
	}
	
	var page_res_id = $('#page_res_id').val();
	//时间控件
	pickDatetimeControl($("#tbx_shop_order_plan_start_time"));
	pickDatetimeControl($("#tbx_shop_order_plan_end_time"));
	
	//保存
	$("form").submit(function() {
		
		var tableCount = $('#customerRejectSfcTable tr').length;
		if (tableCount <= 1) {
			showErrorNoticeMessage("系统提示：客退信息关联内容不能为空");
			return false;
		}
		
		var data = $("form").serialize();	
		var customerRejectSfcData = $.MTable.getTableData("customerRejectSfcTable");
		customerRejectSfcData = JSON.stringify(customerRejectSfcData);
		
		data = data+"&shopOrderFormMap.customerRejectSfcData="+customerRejectSfcData;
		
		$.post(rootPath + "/work_plan/customer_reject_shoporder_maintenance/saveCustomerRejectShoporder.shtml", data, function(e, status, xhr){
			var data= JSON.parse(e);
			if (data.status) {
				showSuccessNoticeMessage("客退单信息保存成功");
			}else {
				showErrorNoticeMessage(data.message);
			}
		},"json")
		return false;
	});
	
	//检索
	$('#btnQuery').click(function (){
		var shoporder_no = $('#tbx_shoporder_no').val();
		var tb = $(".index_centent");
		tb.empty();
		tb.load(rootPath + "/work_plan/customer_reject_shoporder_maintenance/queryCustomerRejectShoporder.shtml?shoporder_no="+shoporder_no+"&page_res_id="+page_res_id);
	})
	
	//清除
	$('#btnClear').click(function (){
		var tb = $(".index_centent");
		tb.empty();
		tb.load(rootPath + "/work_plan/customer_reject_shoporder_maintenance/customer_reject_shoporder_maintenance.shtml?page_res_id="+page_res_id);
	})
	
	//删除
	$('#btnDelete').click(function (){
		$.MsgBox.Confirm("","该条数据将会被删除，确定继续吗？",function (){
			var shoporder_id = $('#shoporder_id').val();
			
			var tb = $(".index_centent");
			tb.empty();
			tb.load(rootPath + "/work_plan/customer_reject_shoporder_maintenance/delCustomerRejectShoporder.shtml?shoporder_id="+shoporder_id+"&page_res_id="+page_res_id);
		});
	})
	
	var checkSfcInfo = function (input){
		var data = "sfc="+$(input).val();
		$.ajax({
            url : rootPath + "/work_plan/customer_reject_shoporder_maintenance/checkSfcInfo.shtml",
            data: data,
            cache : false, 
            async : false,
            type : "POST",
            dataType : 'json',
            success : function (result){
            	var data= JSON.parse(result);
    			if (data.status) {
    				$('#check_shoporder_id').val(data.message);
    			}else {
    				showErrorNoticeMessage(data.message);
    			}
            }
        });
		var v = $('#check_shoporder_id').val();
		$(input).parent().parent().find('td.mtable_head_hide').children().attr('value', v);
	}
	
	//插入新行
	$('#tb_add').click(function (){
		var newRow = "<tr>" +
					"<td><input type='text' validate_allowedempty='N' validate_errormsg='旧SFC不能为空！'></td>" +
					"<td><input type='text' validate_allowedempty='N' validate_errormsg='新SFC不能为空！'></td>" +
					"<td class='mtable_head_hide'><input type='text' validate_allowedempty='N' validate_errormsg='旧工单号不能为空！'></td>" +
				"</tr>";
		$.MTable.addRow("customerRejectSfcTable",newRow);
		$('#customerRejectSfcTable tr:not(:first)').find("td :eq(0)").blur(function (){
			checkSfcInfo(this);
		})
	})
	
	//删除选中行
	$('#tb_del').click(function (){
		$.MTable.delCheckRow("customerRejectSfcTable");
	})
	
	//删除所有行
	$('#tb_delAll').click(function (){
		$.MsgBox.Confirm("系统提示","该操作会清空下列所有行，确定继续吗？",function (){
			$.MTable.delAll("customerRejectSfcTable");
		});
	})
})