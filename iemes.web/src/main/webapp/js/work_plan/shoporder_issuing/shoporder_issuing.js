$(document).ready(function() {
	
	if (!stringIsNull($('#errorMessage').val())) {
		showErrorNoticeMessage($('#errorMessage').val());
	}
	
	if (!stringIsNull($('#successMessage').val())) {
		showSuccessNoticeMessage($('#successMessage').val());
	}
	
	//下达
	$("form").submit(function() {	
		var data = $("form").serialize();		
		
		$.post(rootPath + "/work_plan/shoporder_issuing/issuingShoporder.shtml", data, function(e, status, xhr){
			var data= JSON.parse(e);
			if (data.status) {
				showSuccessNoticeMessage("工单下达成功");
			}else {
				showErrorNoticeMessage(data.message);
			}
		},"json")
		return false;
	});
	
	//检索
	$('#btnQuery').click(function (){
		var shoporder_no = $('#tbx_shoporder_no').val();
		var tb = $(".index_centent");
		tb.empty();
		tb.load(rootPath + "/work_plan/shoporder_issuing/queryShoporder.shtml?shoporder_no="+shoporder_no);
	})
	
	//清除
	$('#btnClear').click(function (){
		var tb = $(".index_centent");
		tb.empty();
		tb.load(rootPath + "/work_plan/shoporder_issuing/shoporder_issuing.shtml");
	})
})

