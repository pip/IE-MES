$(document).ready(function() {
	
	if (!stringIsNull($('#errorMessage').val())) {
		showErrorNoticeMessage($('#errorMessage').val());
	}
	
	if (!stringIsNull($('#successMessage').val())) {
		showSuccessNoticeMessage($('#successMessage').val());
	}
	var page_res_id = $('#page_res_id').val();
	//保存
	$("form").submit(function() {
		var data = $("form").serialize();
		var udata = $.MTable.getTableData("item_table");
		var item_surrenals = $.MTable.getTableDataWithMultiInputsInOneCell("item_surrenal_Table");
		data = data + "&itemFormMap.udata="+JSON.stringify(udata)+"&itemFormMap.item_surrenals="+JSON.stringify(item_surrenals);
		
		$.post(rootPath + "/workcenter_model/item/saveItem.shtml", data, function(e, status, xhr){
			var data= JSON.parse(e);
			if (data.status) {
				showSuccessNoticeMessage("物料保存成功");
			}else {
				showErrorNoticeMessage(data.message);
			}
		},"json")
		return false;
	});
	
	//检索
	$('#btnQuery').click(function (){
		var item_no = $('#tbx_item_no').val();
		var version = $('#tbx_item_version').val();
		var tb = $(".index_centent");
		tb.empty();
		tb.load(rootPath + "/workcenter_model/item/queryItem.shtml?item_no="+item_no+"&version="+version+"&page_res_id="+page_res_id);
	})
	
	//清除
	$('#btnClear').click(function (){
		var tb = $(".index_centent");
		tb.empty();
		tb.load(rootPath + "/workcenter_model/item/item_maintenance.shtml?page_res_id="+page_res_id);
	})
	
	//删除
	$('#btnDelete').click(function (){
		$.MsgBox.Confirm("系统提示","该条数据将会被删除，确定继续吗？",function (){
			var item_id = $('#item_id').val();
			
			var tb = $(".index_centent");
			tb.empty();
			tb.load(rootPath + "/workcenter_model/item/delItem.shtml?item_id="+item_id+"&page_res_id="+page_res_id);
		});
	})
	

	//插入新行
	$('#tb_add').click(function (){
		var num = 0;
		var trs = $('#item_surrenal_Table tr');
		for (var i=1;i<trs.length;i++) {
			var td = trs.eq(i).find("input:first");
			if (td[0]!=undefined) {
				if (td[0].value!=undefined && parseInt(td[0].value)>=num) {
					num = parseInt(td[0].value);
				}
			}
		}
		num += 10;
		var newRow = "<tr><td><input type='text' readonly='readonly' validate_datatype='number_int' validate_errormsg='顺序必须为数字' value="+num+"></td>" +
						"<td><input type='text' class='formText_query majuscule' dataValue='item_no' viewTitle='物料' " +
						"data-url='/popup/queryAllItem.shtml' relationId='popup_item_surrenal_id"+num+"' id='popup_item_surrenal_no"+num+"' " +
						"validate_allowedempty='N' validate_errormsg='替代物料不能为空！'/>" +
						"<input type='hidden' id='popup_item_surrenal_id"+num+"' dataValue='id' relationId='item_surrenal_version"+num+"'>" +
						"<input type='button' submit='N' value='检索' onclick='operationBrowse(this)' textFieldId='popup_item_surrenal_no"+num+"'></td>" +
				
						"<td><input type='text' readonly='readonly' validate_allowedempty='N' id='item_surrenal_version"+num+"' dataValue='item_version' validate_errormsg='版本不能为空！' ></td>" +
						
						"<td><input type='text' class='formText_query majuscule' dataValue='item_no' viewTitle='物料' " +
						"data-url='/popup/queryAllItem.shtml' relationId='item_surrenal_usage_id"+num+"' id='item_surrenal_usage_no"+num+"' " +
						"validate_allowedempty='N' validate_errormsg='有效装配物料不能为空！'/>" +
						"<input type='hidden' id='item_surrenal_usage_id"+num+"' dataValue='id' relationId='item_surrenal_usage_version"+num+"'>" +
						"<input type='button' submit='N' value='检索' onclick='operationBrowse(this)' textFieldId='item_surrenal_usage_no"+num+"'></td>" +
						
						"<td><input type='text' readonly='readonly' validate_allowedempty='N' id='item_surrenal_usage_version"+num+"' dataValue='item_version' validate_errormsg='有效版本不能为空！' ></td>" +
						"<td><input type='text' datetime_format_type='datetime' validate_allowedempty='N' validate_errormsg='有效期自不能为空！' ></td>" +
						"<td><input type='text' datetime_format_type='datetime' validate_allowedempty='N' validate_errormsg='有效期至不能为空！' ></td></tr>";
		$.MTable.addRow("item_surrenal_Table",newRow);
	})
	
	//删除选中行
	$('#tb_del').click(function (){
		$.MTable.delCheckRow("item_surrenal_Table");
	})
	
	//删除所有行
	$('#tb_delAll').click(function (){
		$.MsgBox.Confirm("系统提示","该操作会清空下列所有行，确定继续吗？",function (){
			$.MTable.delAll("item_surrenal_Table");
		});
	})
})

//动态列表内时间控件 
$(document).ready(function(){
    $(".mtable").on("mousemove","[datetime_format_type=datetime]",function(e){ 
        pickDatetimeControl(this);
    })
 })