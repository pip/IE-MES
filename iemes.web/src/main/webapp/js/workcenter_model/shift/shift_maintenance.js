$(document).ready(function() {
	
	if (!stringIsNull($('#errorMessage').val())) {
		showErrorNoticeMessage($('#errorMessage').val());
	}
	
	if (!stringIsNull($('#successMessage').val())) {
		showSuccessNoticeMessage($('#successMessage').val());
	}
	var page_res_id = $('#page_res_id').val();
	//时间控件
	pickDatetimeControl($("#tbx_shift_start_time"));
	pickDatetimeControl($("#tbx_shift_end_time"));
	
	//保存
	$("form").submit(function() {	
		var data = $("form").serialize();
		var userSelectGroups = $('#selectGroups')[0].children;
		var selectedUserList = "";	
		for (var i=0;i<userSelectGroups.length;i++) {
			selectedUserList += userSelectGroups[i].value;
			if (i!=userSelectGroups.length-1) selectedUserList += ",";
		}
		var userUnSelectGroups = $('#groupsForSelect')[0].children;
		var unSelectedUserList = "";
		for (var i=0;i<userUnSelectGroups.length;i++) {
			unSelectedUserList += userUnSelectGroups[i].value;
			if (i!=userUnSelectGroups.length-1) unSelectedUserList += ",";
		}
		
		data = data + "&shiftFormMap.selectedUserList="+selectedUserList;
		data = data + "&shiftFormMap.unSelectedUserList="+unSelectedUserList;
		
		var udefine_data = $.MTable.getTableData("uDData");
		/*udefine_data = JSON.stringify(udefine_data).replace(/"/g,"'");*/
		udefine_data = JSON.stringify(udefine_data);
		
		data = data+"&shiftFormMap.udefined_data="+udefine_data;
		
		$.post(rootPath + "/workcenter_model/shift/saveShift.shtml", data, function(e, status, xhr){
			var data= JSON.parse(e);
			if (data.status) {
				showSuccessNoticeMessage("班次信息保存成功");
			}else {
				//showErrorNoticeMessage("班次信息保存失败："+data.message);
				showErrorNoticeMessage(data.message);
			}
		},"json")
		return false;
	});
	
	//检索
	$('#btnQuery').click(function (){
		var shift_no = $('#tbx_shift_no').val();
		var tb = $(".index_centent");
		tb.empty();
		tb.load(rootPath + "/workcenter_model/shift/queryShift.shtml?shift_no="+shift_no+"&page_res_id="+page_res_id);
	})
	
	//清除
	$('#btnClear').click(function (){
		var tb = $(".index_centent");
		tb.empty();
		tb.load(rootPath + "/workcenter_model/shift/shift_maintenance.shtml?page_res_id="+page_res_id);
	})
	
	$('#btnDelete').click(function (){
		$.MsgBox.Confirm("","该条数据将会被删除，确定继续吗？",function (){
			var shift_id = $('#shift_id').val();
			
			var tb = $(".index_centent");
			tb.empty();
			tb.load(rootPath + "/workcenter_model/shift/delShift.shtml?shift_id="+shift_id+"&page_res_id="+page_res_id);
		});
	})
	
})

