$(document).ready(function() {
	
	if (!stringIsNull($('#errorMessage').val())) {
		showErrorNoticeMessage($('#errorMessage').val());
	}
	
	if (!stringIsNull($('#successMessage').val())) {
		showSuccessNoticeMessage($('#successMessage').val());
	}
	var page_res_id = $('#page_res_id').val();
	//保存
	$("form").submit(function() {	
		var data = $("form").serialize();
		var ncCodeSelectGroups = $('#selectGroups')[0].children;
		var selectedNcCodeList = "";	
		for (var i=0;i<ncCodeSelectGroups.length;i++) {
			selectedNcCodeList += ncCodeSelectGroups[i].value;
			if (i!=ncCodeSelectGroups.length-1) selectedNcCodeList += ",";
		}
		var ncCodeUnSelectGroups = $('#groupsForSelect')[0].children;
		var unSelectedNcCodeList = "";
		for (var i=0;i<ncCodeUnSelectGroups.length;i++) {
			unSelectedNcCodeList += ncCodeUnSelectGroups[i].value;
			if (i!=ncCodeUnSelectGroups.length-1) unSelectedNcCodeList += ",";
		}
		
		data = data + "&ncCodeGroupFormMap.selectedNcCodeList="+selectedNcCodeList;
		data = data + "&ncCodeGroupFormMap.unSelectedNcCodeList="+unSelectedNcCodeList;
		
		$.post(rootPath + "/workcenter_model/nc_code_group/saveNcCodeGroup.shtml", data, function(e, status, xhr){
			var data= JSON.parse(e);
			if (data.status) {
				showSuccessNoticeMessage("不良代码组信息保存成功");
			}else {
				//showErrorNoticeMessage("不良代码组信息保存失败："+data.message);
				showErrorNoticeMessage(data.message);
			}
		},"json")
		return false;
	});
	
	//检索
	$('#btnQuery').click(function (){
		var nc_code_group_no = $('#tbx_nc_code_group_no').val();
		var tb = $(".index_centent");
		tb.empty();
		tb.load(rootPath + "/workcenter_model/nc_code_group/queryNcCodeGroup.shtml?nc_code_group_no="+nc_code_group_no+"&page_res_id="+page_res_id);
	})
	
	//清除
	$('#btnClear').click(function (){
		var tb = $(".index_centent");
		tb.empty();
		tb.load(rootPath + "/workcenter_model/nc_code_group/nc_code_group_maintenance.shtml?page_res_id="+page_res_id);
	})
	
	$('#btnDelete').click(function (){
		$.MsgBox.Confirm("","该条数据将会被删除，确定继续吗？",function (){
			var nc_code_group_id = $('#nc_code_group_id').val();
			
			var tb = $(".index_centent");
			tb.empty();
			tb.load(rootPath + "/workcenter_model/nc_code_group/delNcCodeGroup.shtml?nc_code_group_id="+nc_code_group_id+"&page_res_id="+page_res_id);
		});
	})
})