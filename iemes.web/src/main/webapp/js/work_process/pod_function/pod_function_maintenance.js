$(document).ready(function() {
	
	if (!stringIsNull($('#errorMessage').val())) {
		showErrorNoticeMessage($('#errorMessage').val());
	}
	
	if (!stringIsNull($('#successMessage').val())) {
		showSuccessNoticeMessage($('#successMessage').val());
	}
	
	//保存
	$("form").submit(function() {
		var data = $("form").serialize();
		$.post(rootPath + "/work_process/pod_function/savePodFunction.shtml", data, function(e, status, xhr){
			var data= JSON.parse(e);
			if (data.status) {
				showSuccessNoticeMessage("POD功能保存成功");
			}else {
				showErrorNoticeMessage(data.message);
			}
		},"json")
		return false;
	});
	
	//检索
	$('#btnQuery').click(function (){
		var pod_function_no = $('#tbx_pod_function_no').val();
		var tb = $(".index_centent");
		tb.empty();
		tb.load(rootPath + "/work_process/pod_function/queryPodFunction.shtml?pod_function_no="+pod_function_no);
	})
	
	//清除
	$('#btnClear').click(function (){
		var tb = $(".index_centent");
		tb.empty();
		tb.load(rootPath + "/work_process/pod_function/pod_function_maintenance.shtml");
	})
	
	//删除
	$('#btnDelete').click(function (){
		$.MsgBox.Confirm("系统提示","该条数据将会被删除，确定继续吗？",function (){
			var pod_function_id = $('#pod_function_id').val();
			var tb = $(".index_centent");
			tb.empty();
			tb.load(rootPath + "/work_process/pod_function/delPodFunction.shtml?pod_function_id="+pod_function_id);
		});
	})
})