$(document).ready(function() {
	
	if (!stringIsNull($('#errorMessage').val())) {
		showErrorNoticeMessage($('#errorMessage').val());
	}
	
	if (!stringIsNull($('#successMessage').val())) {
		showSuccessNoticeMessage($('#successMessage').val());
	}
	
	//保存
	$("form").submit(function() {
		var data = $("form").serialize();
		var pod_buttons = $.MTable.getTableDataWithMultiInputsInOneCell("pod_panel_botton_table");
		data = data +"&podPanelFormMap.buttons="+JSON.stringify(pod_buttons);
		
		$.post(rootPath + "/work_process/pod_panel/savePodPanel.shtml", data, function(e, status, xhr){
			var data= JSON.parse(e);
			if (data.status) {
				showSuccessNoticeMessage("POD面板保存成功");
			}else {
				showErrorNoticeMessage(data.message);
			}
		},"json")
		return false;
	});
	
	//检索
	$('#btnQuery').click(function (){
		var pod_panel_no = $('#tbx_pod_panel_no').val();
		var tb = $(".index_centent");
		tb.empty();
		tb.load(rootPath + "/work_process/pod_panel/queryPodPanel.shtml?pod_panel_no="+pod_panel_no);
	})
	
	//清除
	$('#btnClear').click(function (){
		var tb = $(".index_centent");
		tb.empty();
		tb.load(rootPath + "/work_process/pod_panel/pod_panel_maintenance.shtml");
	})
	
	//删除
	$('#btnDelete').click(function (){
		$.MsgBox.Confirm("系统提示","该条数据将会被删除，确定继续吗？",function (){
			var pod_panel_id = $('#pod_panel_id').val();
			var tb = $(".index_centent");
			tb.empty();
			tb.load(rootPath + "/work_process/pod_panel/delPodPanel.shtml?pod_panel_id="+pod_panel_id);
		});
	})
	
	//插入新行
	$('#tb_add').click(function (){
		var num = 0;
		var trs = $('#pod_panel_botton_table tr');
		for (var i=1;i<trs.length;i++) {
			var td = trs.eq(i).find("input:first");
			if (td[0]!=undefined) {
				if (td[0].value!=undefined && parseInt(td[0].value)>=num) {
					num = parseInt(td[0].value);
				}
			}
		}
		num += 10;
		var newRow = "<tr>" +
				"<td>	<input type='text' readonly='readonly' validate_datatype='number_int' validate_errormsg='顺序必须为数字' value='"+num+"'></td>" +
				"<td>	<input type='text' class='formText_query majuscule' datavalue='pod_button_no' viewtitle='POD按钮' data-url='/popup/queryAllPodButtons.shtml' 	relationid='popup_pod_button_id"+num+"'	id='popup_pod_button_no"+num+"' validate_allowedempty='N' validate_errormsg='POD按钮不能为空！'>	<input type='hidden' id='popup_pod_button_id"+num+"' datavalue='id' relationid='popup_pod_button_name"+num+"'>	<input type='button' submit='N' value='检索' onclick='operationBrowse(this)' textfieldid='popup_pod_button_no"+num+"'></td>" +
				"<td>	<input type='text' readonly='readonly' validate_allowedempty='N' id='popup_pod_button_name"+num+"' datavalue='pod_button_name' validate_errormsg='POD名称不能为空！' relationid='popup_pod_buttion_create_user"+num+"'></td>" +
				"<td>	<input type='text' readonly='readonly' validate_allowedempty='N' id='popup_pod_buttion_create_user"+num+"' datavalue='create_user' validate_errormsg='创建人不能为空！'></td>" +
				"</tr>";
		$.MTable.addRow("pod_panel_botton_table",newRow);
	})
	
	//删除选中行
	$('#tb_del').click(function (){
		$.MTable.delCheckRow("pod_panel_botton_table");
	})
	
	//删除所有行
	$('#tb_delAll').click(function (){
		$.MsgBox.Confirm("系统提示","该操作会清空下列所有行，确定继续吗？",function (){
			$.MTable.delAll("pod_panel_botton_table");
		});
	})
})