$(document).ready(function() {
	
	//保存
	$("#record_nccode_form").submit(function() {
		var data = $("form").serialize();
		
		$.post(rootPath + "/work_process/work_pod_panel/recordNcCode.shtml", data, function(e, status, xhr){
			var data= JSON.parse(e);
			if (data.status) {
				showSuccessNoticeMessage("SFC已记录不合格");
			}else {
				showErrorNoticeMessage(data.message);
			}
		},"json")
		return false;
	});
	
	$('#bt_record_nccode').click(function (){
		$('#record_nccode_form').submit();
	})
});
