$(document).ready(function() {
	var dbClickItemList = function(row){
		var data = "param=assemble&assemble=" +JSON.stringify(row);
		$.post(rootPath + "/work_process/work_pod_panel/saveParam.shtml", data, function(e, status, xhr){
			var result= JSON.parse(e);
			if (result.status) {
				var pod_assemble_div = $('#pod_assemble_div');
				pod_assemble_div.empty();
				pod_assemble_div.load(rootPath + "/work_process/work_pod_panel/assemble_ui.shtml");
				$('div.pod_assemble_panel').css({display : "block"});
			}else {
				showErrorNoticeMessage(result.message);
			}
		},"json")
	};
	$.MTable.clickFun2("assemble_item_list_table",dbClickItemList, 0, 0);
});
