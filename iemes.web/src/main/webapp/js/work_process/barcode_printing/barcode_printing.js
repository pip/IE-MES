$(document).ready(function() {
	
	if (!stringIsNull($('#errorMessage').val())) {
		showErrorNoticeMessage($('#errorMessage').val());
	}
	
	if (!stringIsNull($('#successMessage').val())) {
		showSuccessNoticeMessage($('#successMessage').val());
	}
	
	//检索
	$('#btnQuery').click(function (){
		var shopOrderNo = $('#tbx_shop_order_no').val();
		var workcenterId = $('#input_workcenter_id').val();
		var workLineId = $('#input_workline_id').val();
		var workcenterNo = $('#tbx_workcenter_no').val();
		var worklineNo = $('#tbx_workline_no').val();
		if (shopOrderNo==undefined || shopOrderNo==null || shopOrderNo=="") {
			showErrorNoticeMessage("工单不能为空！！！");
			return;
		}
		
		var data = "shop_order_no="+shopOrderNo+"&workcenter_id="+workcenterId+"&workline_id="+workLineId+"&workcenter_no="+workcenterNo+"&workline_no="+worklineNo
		var tb = $(".index_centent");
		tb.empty();
		tb.load(rootPath + "/work_process/barcode_printing/queryShopOrderSfcList.shtml?"+data);
	})
	
	//清除
	$('#btnClear').click(function (){
		var tb = $(".index_centent");
		tb.empty();
		tb.load(rootPath + "/work_process/barcode_printing/barcode_printing.shtml");
	})
	
})