$(document).ready(function() {
	
	if (!stringIsNull($('#errorMessage').val())) {
		showErrorNoticeMessage($('#errorMessage').val());
	}
	
	if (!stringIsNull($('#successMessage').val())) {
		showSuccessNoticeMessage($('#successMessage').val());
	}
	
	$('#div_item_receive_number').css({'display' : 'none'});
	
	//接收
	$("form").submit(function() {	
		var data = $("form").serialize();		
		$.post(rootPath + "/workshop_inventory/workshop_inventory_reception/receptItem.shtml", data, function(e, status, xhr){
			var data= JSON.parse(e);
			if (data.status) {
				showSuccessNoticeMessage("车间库存接收成功");
			}else {
				showErrorNoticeMessage(data.message);
			}
		},"json")
		return false;
	});
	
	//清除
	$('#btnClear').click(function (){
		var tb = $(".index_centent");
		tb.empty();
		tb.load(rootPath + "/workshop_inventory/workshop_inventory_reception/workshop_inventory_reception.shtml");
	})
})

function chooseItemSaveType(row){
	var item_save_type = row.item_save_type;
	if (item_save_type=='batch') {
		$('#inventory_item').text("物料批次:");
		$('#tbx_item_sfc').attr("name", "workshopInventoryFormMap.item_inner_batch");
		$('#item_save_type').val(item_save_type);
		$('#div_item_receive_number').css({'display' : 'block'});
	}
	if (item_save_type=='unit') {
		$('#inventory_item').text("物料SFC:");
		$('#tbx_item_sfc').attr("name", "workshopInventoryFormMap.item_sfc");
		$('#item_save_type').val(item_save_type);
		$('#div_item_receive_number').css({'display' : 'none'});
	}
}

