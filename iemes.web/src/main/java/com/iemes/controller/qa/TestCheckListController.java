package com.iemes.controller.qa;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.iemes.controller.index.BaseController;
import com.iemes.util.Common;


@Controller
@RequestMapping("/qa/test_check_list/")
public class TestCheckListController extends BaseController {
	
	private String backUrl = Common.BACKGROUND_PATH + "/qa/test_check_list/test_check_list";
	
	@RequestMapping("test_check_list")
	public String test_check_list() {
		return backUrl;
	}

}
