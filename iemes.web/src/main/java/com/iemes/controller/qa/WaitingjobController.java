package com.iemes.controller.qa;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.iemes.controller.index.BaseController;
import com.iemes.util.Common;


@Controller
@RequestMapping("/qa/waitingjob/")
public class WaitingjobController extends BaseController {
	
	private String backUrl = Common.BACKGROUND_PATH + "/qa/waitingjob/waitingjob";
	
	@RequestMapping("list")
	public String waitingJobList() {
		return backUrl;
	}

}
