package com.iemes.controller.qa;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.iemes.controller.index.BaseController;
import com.iemes.util.Common;


@Controller
@RequestMapping("/qa/finishedjob/")
public class FinishedjobController extends BaseController {
	
	private String backUrl = Common.BACKGROUND_PATH + "/qa/finishedjob/finishedjob";
	
	@RequestMapping("list")
	public String finishedJobList() {
		return backUrl;
	}

}
