package com.iemes.controller.send_test;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.iemes.controller.index.BaseController;
import com.iemes.util.Common;


@Controller
@RequestMapping("/send_test/site_send_test/")
public class SendTestController extends BaseController {
	
	private String list_url = Common.BACKGROUND_PATH + "/send_test/site_send_test/site_send_test";
	
	private String reliability_report_url = Common.BACKGROUND_PATH + "/send_test/site_send_test/reliability_report";
	
	@RequestMapping("list")
	public String siteSendTestList() {
		return list_url;
	}
	
	@RequestMapping("reliability_report")
	public String reliability_report() {
		return reliability_report_url;
	}

}
