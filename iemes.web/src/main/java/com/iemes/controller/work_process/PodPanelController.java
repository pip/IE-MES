package com.iemes.controller.work_process;

import java.util.List;

import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.iemes.annotation.SystemLog;
import com.iemes.controller.index.BaseController;
import com.iemes.entity.OperationFormMap;
import com.iemes.entity.PodButtonFormMap;
import com.iemes.entity.PodPanelButtonFormMap;
import com.iemes.entity.PodPanelFormMap;
import com.iemes.entity.WorkResourceFormMap;
import com.iemes.exception.BusinessException;
import com.iemes.service.work_process.PodPanelService;
import com.iemes.util.Common;
import com.iemes.util.DateUtils;
import com.iemes.util.ListUtils;
import com.iemes.util.ResponseHelp;
import com.iemes.util.ShiroSecurityHelper;

@Controller
@RequestMapping("/work_process/pod_panel/")
public class PodPanelController extends BaseController {

	private Logger log = Logger.getLogger(this.getClass());
	
	@Inject
	private PodPanelService podPanelService;
	
	private final String podPanelMaintenanceUrl = Common.BACKGROUND_PATH + "/work_process/pod_panel/pod_panel_maintenance";
	
	@RequestMapping("pod_panel_maintenance")
	public String pod_panel_maintenance(Model model) throws Exception {
		return podPanelMaintenanceUrl;
	}
	
	/**
	 * 保存
	 * @param model
	 * @param request
	 * @return
	 */
	@ResponseBody
	@RequestMapping("savePodPanel")
	@SystemLog(module="生产过程管理",methods="生产操作员-面板-保存生产操作员-面板")		//凡需要处理业务逻辑的.都需要记录操作日志
	public String savePodPanel(Model model,HttpServletRequest request){
		PodPanelFormMap podPanelFormMap = getFormMap(PodPanelFormMap.class);
		podPanelFormMap.put("create_time", DateUtils.getStringDateTime());
		podPanelFormMap.put("create_user", ShiroSecurityHelper.getCurrentUsername());
		podPanelFormMap.put("site_id", ShiroSecurityHelper.getSiteId());
		try {
			podPanelService.savePodPanel(podPanelFormMap);
		}catch(BusinessException e) {
			log.error(e.getMessage());
			model.addAttribute(e.getMessage());
			return ResponseHelp.responseErrorText(e.getMessage());
		}
		return ResponseHelp.responseText();
	}
	
	/**
	 * 检索生产操作员-面板
	 * @param model
	 * @param request
	 * @return
	 */
	@RequestMapping("queryPodPanel")
	public String queryPodPanel(Model model,HttpServletRequest request) {
		String podPanelNo = request.getParameter("pod_panel_no");
		try {
			//查询生产操作员-面板
			PodPanelFormMap podPanelFormMap = new PodPanelFormMap();
			podPanelFormMap.put("pod_panel_no", podPanelNo);
			podPanelFormMap.put("site_id", ShiroSecurityHelper.getSiteId());
			List<PodPanelFormMap> list = baseMapper.findByNames(podPanelFormMap);
			
			if (!ListUtils.isNotNull(list)) {
				log.error("未检索到生产操作员-面板编号为："+podPanelNo.toUpperCase()+"的信息");
				model.addAttribute("errorMessage","未检索到生产操作员-面板编号为："+podPanelNo.toUpperCase()+"的信息");
				return podPanelMaintenanceUrl;
			}
			PodPanelFormMap podPanelFormMap2 = list.get(0);
			
			String operationId = podPanelFormMap2.getStr("default_operation");
			String resourceId = podPanelFormMap2.getStr("default_resource");
			String id = podPanelFormMap2.getStr("id");
			
			if (!StringUtils.isEmpty(operationId)) {
				List<OperationFormMap> listOperationFormMap = baseMapper.findByAttribute("id", operationId, OperationFormMap.class);
				if (!ListUtils.isNotNull(listOperationFormMap)) {
					log.error("未检索到对应的默认操作信息，请确认对应的操作是否被删除或不存在");
					model.addAttribute("errorMessage","未检索到对应的默认操作信息，请确认对应的操作是否被删除或不存在");
					return podPanelMaintenanceUrl;
				}
				model.addAttribute("operationFormMap", listOperationFormMap.get(0));
			}
			
			if (!StringUtils.isEmpty(resourceId)) {
				List<WorkResourceFormMap> listWorkResourceFormMap = baseMapper.findByAttribute("id", resourceId, WorkResourceFormMap.class);
				if (!ListUtils.isNotNull(listWorkResourceFormMap)) {
					log.error("未检索到对应的默认资源信息，请确认对应的资源是否被删除或不存在");
					model.addAttribute("errorMessage","未检索到对应的默认资源信息，请确认对应的资源是否被删除或不存在");
					return podPanelMaintenanceUrl;
				}
				model.addAttribute("workResourceFormMap", listWorkResourceFormMap.get(0));
			}
			
			PodPanelButtonFormMap podPanelButtonFormMap2 = new PodPanelButtonFormMap();
			podPanelButtonFormMap2.put("pod_panel_id", id);
			podPanelButtonFormMap2.put("site_id", ShiroSecurityHelper.getSiteId());
			podPanelButtonFormMap2.put("orderby", "order by pod_panel_button_level asc");
			
			List<PodPanelButtonFormMap> listPodPanelButtonFormMap = baseMapper.findByNames(podPanelButtonFormMap2);
			if (ListUtils.isNotNull(listPodPanelButtonFormMap)) {
				for (PodPanelButtonFormMap podPanelButtonFormMap : listPodPanelButtonFormMap) {
					String buttonId = podPanelButtonFormMap.getStr("pod_panel_button_id");
					List<PodButtonFormMap> listPodButtonFormMap = baseMapper.findByAttribute("id", buttonId, PodButtonFormMap.class);
					PodButtonFormMap podButtonFormMap = listPodButtonFormMap.get(0);
					podPanelButtonFormMap.put("pod_button_no", podButtonFormMap.getStr("pod_button_no"));
					podPanelButtonFormMap.put("pod_button_id", podButtonFormMap.getStr("id"));
					podPanelButtonFormMap.put("pod_button_name", podButtonFormMap.getStr("pod_button_name"));
					podPanelButtonFormMap.put("user", podButtonFormMap.getStr("create_user"));
				}
				model.addAttribute("listPodPanelButtonFormMap", listPodPanelButtonFormMap);
			}
			
			model.addAttribute("podPanelFormMap", podPanelFormMap2);
		}catch(Exception e) {
			log.error(e.getMessage());
			model.addAttribute("errorMessage", e.getMessage());
			return podPanelMaintenanceUrl;
		}
		model.addAttribute("successMessage", "检索成功");
		return podPanelMaintenanceUrl;
	}
	
	/**
	 * 删除生产操作员-面板
	 * @param model
	 * @param request
	 * @return
	 */
	@RequestMapping("delPodPanel")
	public String delPodPanel(Model model,HttpServletRequest request) {
		String podPanelId = request.getParameter("pod_panel_id");
		try {
			if (StringUtils.isEmpty(podPanelId)) {
				throw new BusinessException("操作失败，参数不能为空！");
			}
			podPanelService.delPodPanel(podPanelId);
		}catch(BusinessException e) {
			log.error(e.getMessage(), e);
			model.addAttribute("errorMessage", e.getMessage());
			return podPanelMaintenanceUrl;
		}
		model.addAttribute("successMessage", "删除成功");
		return podPanelMaintenanceUrl;
	}
}
