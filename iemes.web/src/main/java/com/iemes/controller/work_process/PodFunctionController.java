package com.iemes.controller.work_process;

import java.util.List;

import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.iemes.annotation.SystemLog;
import com.iemes.controller.index.BaseController;
import com.iemes.entity.PodFunctionFormMap;
import com.iemes.exception.BusinessException;
import com.iemes.service.work_process.PodFunctionService;
import com.iemes.util.Common;
import com.iemes.util.DateUtils;
import com.iemes.util.ListUtils;
import com.iemes.util.ResponseHelp;
import com.iemes.util.ShiroSecurityHelper;

@Controller
@RequestMapping("/work_process/pod_function/")
public class PodFunctionController extends BaseController {

	private Logger log = Logger.getLogger(this.getClass());
	
	@Inject
	private PodFunctionService podFunctionService;
	
	private final String podFunctionMaintenanceUrl = Common.BACKGROUND_PATH + "/work_process/pod_function/pod_function_maintenance";
	
	@RequestMapping("pod_function_maintenance")
	public String pod_function_maintenance(Model model) throws Exception {
		return podFunctionMaintenanceUrl;
	}
	
	@ResponseBody
	@RequestMapping("savePodFunction")
	@SystemLog(module="生产过程管理",methods="生产操作员-功能-保存生产操作员-功能")		//凡需要处理业务逻辑的.都需要记录操作日志
	public String savePodFunction(Model model,HttpServletRequest request){
		PodFunctionFormMap podFunctionFormMap = getFormMap(PodFunctionFormMap.class);
		podFunctionFormMap.put("create_time", DateUtils.getStringDateTime());
		podFunctionFormMap.put("create_user", ShiroSecurityHelper.getCurrentUsername());
		podFunctionFormMap.put("site_id", ShiroSecurityHelper.getSiteId());
		try {
			podFunctionService.savePodFunction(podFunctionFormMap);
		}catch(BusinessException e) {
			log.error(e.getMessage());
			model.addAttribute(e.getMessage());
			return ResponseHelp.responseErrorText(e.getMessage());
		}
		return ResponseHelp.responseText();
	}
	
	/**
	 * 检索生产操作员-功能
	 * @param model
	 * @param request
	 * @return
	 */
	@RequestMapping("queryPodFunction")
	public String queryPodFunction(Model model,HttpServletRequest request) {
		String podFunctionNo = request.getParameter("pod_function_no");
		try {
			//查询生产操作员-功能
			PodFunctionFormMap podFunctionFormMap = new PodFunctionFormMap();
			podFunctionFormMap.put("pod_function_no", podFunctionNo);
			podFunctionFormMap.put("site_id", ShiroSecurityHelper.getSiteId());
			List<PodFunctionFormMap> list = baseMapper.findByNames(podFunctionFormMap);
			
			if (!ListUtils.isNotNull(list)) {
				log.error("未检索到生产操作员-功能编号编号为："+podFunctionNo.toUpperCase()+"的信息");
				model.addAttribute("errorMessage","未检索到生产操作员-功能编号为："+podFunctionNo.toUpperCase()+"的信息");
				return podFunctionMaintenanceUrl;
			}
			PodFunctionFormMap podFunctionFormMap2 = list.get(0);
			model.addAttribute("podFunctionFormMap", podFunctionFormMap2);
		}catch(Exception e) {
			log.error(e.getMessage());
			model.addAttribute("errorMessage", e.getMessage());
			return podFunctionMaintenanceUrl;
		}
		model.addAttribute("successMessage", "检索成功");
		return podFunctionMaintenanceUrl;
	}
	
	/**
	 * 删除生产操作员-功能
	 * @param model
	 * @param request
	 * @return
	 */
	@RequestMapping("delPodFunction")
	public String delPodFunction(Model model,HttpServletRequest request) {
		String podFunctionId = request.getParameter("pod_function_id");
		try {
			if (StringUtils.isEmpty(podFunctionId)) {
				throw new BusinessException("操作失败，参数不能为空！");
			}
			podFunctionService.delPodFunction(podFunctionId);
		}catch(BusinessException e) {
			log.error(e.getMessage(), e);
			model.addAttribute("errorMessage", e.getMessage());
			return podFunctionMaintenanceUrl;
		}
		model.addAttribute("successMessage", "删除成功");
		return podFunctionMaintenanceUrl;
	}
}
